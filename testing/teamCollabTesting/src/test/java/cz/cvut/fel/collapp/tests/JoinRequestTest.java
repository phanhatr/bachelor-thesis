package cz.cvut.fel.collapp.tests;

import com.microsoft.playwright.*;
import com.microsoft.playwright.options.AriaRole;
import com.microsoft.playwright.options.LoadState;
import cz.cvut.fel.collapp.tests.config.References;
import org.junit.jupiter.api.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import java.nio.file.Paths;

@TestMethodOrder(OrderAnnotation.class)
public class JoinRequestTest {
    // Shared between all tests in this class.
    static Playwright playwright;
    static Browser browser;

    // New instance for each test method.
    BrowserContext context;
    Page page;


    @BeforeAll
    static void launchBrowser() {
        playwright = Playwright.create();
        // Choose which browser, headless or not
        browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
    }

    @AfterAll
    static void closeBrowser() {
        playwright.close();
    }

    @BeforeEach
    void createContextAndPage() {
        //open with saved state in appLoginInfo.json
        context = browser.newContext(new Browser.NewContextOptions().setStorageStatePath(Paths.get("appLoginInfo.json")));
        page = context.newPage();
    }

    @AfterEach
    void closeContext() {
        context.close();
    }

    @Test
    @Order(1)
    public void TestA_CheckJoinRequests_WhenProjectIsJustCreated_EmptyJoinRequests() {
        page.navigate(References.getUrl() + "secured/home.html");
        page.getByPlaceholder("Email address").fill("playwright3@testing.ui.com");
        page.getByPlaceholder("Password").fill("Secret12!");
        page.getByPlaceholder("Password").press("Enter");

        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("New project")).click();
        page.getByPlaceholder("Enter project title (required)").fill("Playwright app testing");
        page.getByPlaceholder("Enter number of wanted members (required)").fill("2");
        page.getByLabel("other").check();
        page.getByPlaceholder("Write few sentences about the project.").fill("I am seeking people like me.");
        page.getByRole(AriaRole.GROUP).filter(new Locator.FilterOptions().setHasText("Wanted positions * Wanted positionsAnyFrontend developerBackend developerDesigne")).locator("div").nth(1).click();
        page.getByRole(AriaRole.LISTITEM).filter(new Locator.FilterOptions().setHasText("Any")).click();
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Publish project")).click();
        page.onceDialog(dialog -> {
            System.out.printf("Dialog message: %s%n", dialog.message());
            dialog.dismiss();
        });
        page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("My projects")).click();
        page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Playwright app testing Any 1 / 2 I am seeking people like me. View Project")).click();
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("View requests")).click();
        Assertions.assertTrue(page.getByText("There are no requests to join the project").isVisible());
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Close")).click();
    }

    @Test
    @Order(2)
    public void TestB_SendJoinRequest_WhenAllFieldsHaveValidValues_JoinRequestPending() {
        page.navigate(References.getUrl() + "secured/home.html");
        page.getByPlaceholder("Email address").fill("playwright@tester.com");
        page.getByPlaceholder("Password").fill("Secret12!");
        page.getByPlaceholder("Password").press("Enter");

        page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Playwright app testing Any 1 / 2 I am seeking people like me. View Project")).click();
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Request to join")).click();
        page.getByPlaceholder("Write a few sentences about yourself.").fill("I am just like you. Please take me in.");
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Send request")).click();
        page.onceDialog(dialog -> {
            System.out.printf("Dialog message: %s%n", dialog.message());
            dialog.dismiss();
        });
        page.waitForLoadState(LoadState.NETWORKIDLE); //Solves the problem
        Assertions.assertTrue(page.getByText("Your request is waiting to be resolved.").isVisible());
    }

    @Test
    @Order(3)
    public void TestC_AcceptJoinRequest_WhenThereIsJoinRequest_NewMemberAdded() {
        page.navigate(References.getUrl() + "secured/home.html");
        page.getByPlaceholder("Email address").fill("playwright3@testing.ui.com");
        page.getByPlaceholder("Password").fill("Secret12!");
        page.getByPlaceholder("Password").press("Enter");

        page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("My projects")).click();
        page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Playwright app testing Any 1 / 2 I am seeking people like me. View Project")).click();
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("View requests")).click();

        String currentUser = page.waitForSelector(".name-box").textContent();
        Assertions.assertEquals(References.getFullName(), currentUser);
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Accept")).click();
        page.waitForLoadState(LoadState.NETWORKIDLE); //Solves the problem
        Assertions.assertTrue(page.getByRole(AriaRole.MAIN).locator("div")
                .filter(new Locator.FilterOptions().setHasText("Playwright Positive MEMBER playwright@tester.com")).
                nth(2).isVisible());

    }

}