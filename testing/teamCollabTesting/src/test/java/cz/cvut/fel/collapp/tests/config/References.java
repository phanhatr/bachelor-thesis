package cz.cvut.fel.collapp.tests.config;

public class References {
    private static final String DEV_URL = "http://localhost:8080/";
    private static final String FIRST_NAME = "UI";
    private static final String LAST_NAME = "Tester";

    public static String getUrl() {
        return DEV_URL;
    }

    public static String getFirstName() {
        return FIRST_NAME;
    }

    public static String getLastName() {
        return LAST_NAME;
    }

    public static String getFullName() {
        return FIRST_NAME + " " + LAST_NAME;
    }


}
