package cz.cvut.fel.collapp.tests;

import com.microsoft.playwright.*;
import com.microsoft.playwright.options.AriaRole;
import cz.cvut.fel.collapp.tests.config.References;
import org.junit.jupiter.api.*;

import java.nio.file.Paths;


public class A001GenerateSessionTest {
    // Shared between all tests in this class.
    static Playwright playwright;
    static Browser browser;

    // New instance for each test method.
    BrowserContext context;
    Page page;

    @BeforeAll
    static void launchBrowser() {
        playwright = Playwright.create();
        // Choose which browser, headless or not
        browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
    }

    @AfterAll
    static void closeBrowser() {
        playwright.close();
    }

    @BeforeEach
    void createContextAndPage() {
        context = browser.newContext();
        page = context.newPage();
    }

    @Test
    public void Authentication_WhenIsSuccessful_CanViewHomepageAsLoggedInUser() {
        page.navigate(References.getUrl() + "secured/home.html");

        page.getByRole(AriaRole.HEADING).textContent();
        page.getByPlaceholder("Email address").fill("admin@collab-test.com");
        page.getByPlaceholder("Password").fill("Adm!n12!");
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Sign in")).click();

        String currentUser = page.waitForSelector(".name-box").textContent();
        Assertions.assertEquals(References.getFullName(), currentUser);
        context.storageState(new BrowserContext.StorageStateOptions().setPath(Paths.get("appLoginInfo.json")));
    }


}