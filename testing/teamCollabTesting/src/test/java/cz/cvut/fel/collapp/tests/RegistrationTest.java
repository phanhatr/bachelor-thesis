package cz.cvut.fel.collapp.tests;

import com.microsoft.playwright.*;
import com.microsoft.playwright.options.AriaRole;
import cz.cvut.fel.collapp.tests.config.References;
import org.junit.jupiter.api.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import java.nio.file.Paths;

@TestMethodOrder(OrderAnnotation.class)
public class RegistrationTest {
    // Shared between all tests in this class.
    static Playwright playwright;
    static Browser browser;

    // New instance for each test method.
    BrowserContext context;
    Page page;


    @BeforeAll
    static void launchBrowser() {
        playwright = Playwright.create();
        // Choose which browser, headless or not
        browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
    }

    @AfterAll
    static void closeBrowser() {
        playwright.close();
    }

    @BeforeEach
    void createContextAndPage() {
        //open with saved state in appLoginInfo.json
        context = browser.newContext(new Browser.NewContextOptions().setStorageStatePath(Paths.get("appLoginInfo.json")));
        page = context.newPage();
    }

    @AfterEach
    void closeContext() {
        context.close();
    }

    @Test
    @Order(1)
    public void TestA_RegisterAccount_WhenAllFieldsHaveValidValues_AccountIsRegistered() {
        page.navigate(References.getUrl());
        page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Get started")).click();
        page.getByPlaceholder("Email address").click();
        page.getByPlaceholder("Email address").fill("playwright3@testing.ui.com");
        page.getByPlaceholder("First name").click();
        page.getByPlaceholder("First name").fill(References.getFirstName());
        page.getByPlaceholder("Last name").click();
        page.getByPlaceholder("Last name").fill(References.getLastName());
        page.getByPlaceholder("Tell us something about yourself").click();
        page.getByPlaceholder("Tell us something about yourself").fill("Testing GUI.");
        page.getByPlaceholder("Password", new Page.GetByPlaceholderOptions().setExact(true)).click();
        page.getByPlaceholder("Password", new Page.GetByPlaceholderOptions().setExact(true)).fill("Secret12!");
        page.getByPlaceholder("Repeat your password").click();
        page.getByPlaceholder("Repeat your password").fill("Secret12!");
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Register")).click();
        page.onceDialog(dialog -> {
            Assertions.assertEquals( "User registration successful", dialog.message());
            dialog.dismiss();
        });
    }

    @Test
    @Order(2)
    public void TestB_Authentication_WhenIsSuccessful_CanViewHomepageAsLoggedInUser() {
        page.navigate(References.getUrl() + "secured/home.html");

        page.getByRole(AriaRole.HEADING).textContent();
        page.getByPlaceholder("Email address").fill("playwright3@testing.ui.com");
        page.getByPlaceholder("Password").fill("Secret12!");
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Sign in")).click();

        String currentUser = page.waitForSelector(".name-box").textContent();
        Assertions.assertEquals(References.getFullName(), currentUser);
    }

}