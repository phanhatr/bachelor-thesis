package cz.cvut.fel.collapp.tests;

import com.microsoft.playwright.*;
import com.microsoft.playwright.options.AriaRole;
import com.microsoft.playwright.options.LoadState;
import cz.cvut.fel.collapp.tests.config.References;
import org.junit.jupiter.api.*;

import java.nio.file.Paths;

public class ProjectCreationTest {
    // Shared between all tests in this class.
    static Playwright playwright;
    static Browser browser;

    // New instance for each test method.
    BrowserContext context;
    Page page;


    @BeforeAll
    static void launchBrowser() {
        playwright = Playwright.create();
        // Choose which browser, headless or not
        browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
    }

    @AfterAll
    static void closeBrowser() {
        playwright.close();
    }

    @BeforeEach
    void createContextAndPage() {
        //open with saved state in appLoginInfo.json
        context = browser.newContext(new Browser.NewContextOptions().setStorageStatePath(Paths.get("appLoginInfo.json")));
        page = context.newPage();
    }

    @AfterEach
    void closeContext() {
        context.close();
    }

    @Test
    public void CreateProject_WhenAllFieldsHaveValidValues_ProjectIsCreated() {
        page.navigate(References.getUrl() + "secured/myprojects.html");
        page.waitForLoadState(LoadState.NETWORKIDLE); // Solves the problem

        Assertions.assertEquals("My projects", page.getByRole(AriaRole.HEADING).textContent());
        int myProjectsCount = page.locator("c-project-item").count();


        page.navigate(References.getUrl() + "secured/home.html");
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("New project")).click();
        Assertions.assertEquals("Create a new project", page.getByRole(AriaRole.HEADING).textContent());

        //fill form
        page.locator("input#project-title").fill("Creating a project with Playwright");
        page.getByPlaceholder("Enter project title (required)").fill("Creating a project with Playwright");

        page.locator("input#max-members").fill("6");
        page.getByLabel("school").check();
        page.locator("textarea#project-description").fill("I am testing with Playwright.");
        page.waitForSelector(".selectMultiple").click();
        page.getByRole(AriaRole.LISTITEM).filter(new Locator.FilterOptions().setHasText("Any")).click();

        //submit
        page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Publish project")).click();

        //check
        page.navigate(References.getUrl() + "secured/myprojects.html");
        page.waitForLoadState(LoadState.NETWORKIDLE); //Solves the problem
        Assertions.assertEquals("My projects", page.getByRole(AriaRole.HEADING).textContent());
        int myProjectsCountAfterCreation = page.locator("c-project-item").count();

        Assertions.assertEquals(myProjectsCount + 1, myProjectsCountAfterCreation);
    }
}
