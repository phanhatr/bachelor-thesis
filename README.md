# Bachelor thesis - Team Collab

- program can be run with the rebuild.sh script, which runs it via docker-compose

    \bachelor-thesis\backend\rebuild.sh

## Project Description

Semestrální práce se často dělají ve dvojicích, případně větších týmech. Nicméně na technických školách bývá problém domluva mezi studenty. Navrhovaný systém by měl poskytnout studentům prostředí, kde by mohli rychle a snadno najít kolegy do týmu. Systém by měl umožňovat i skládání heterogenních týmů, kdy je možné specifikovat, kolik lidí je třeba a jaké mají mít schopnosti (znalost nějaké domény, programovacího jazyka, technologie, množství volného času).
Každý projekt mi svůj vlastní popis a specifikace (čeho se týká - např. předmět, v čem je implementován ap.) a podle těchto kritérií by měl být vyhledatelný.
Do budoucna by takový systém mohl sloužit např. opensourcovým projektům při hledání dobrovolníků a na druhou stranu začátečníkům v programování, kde si mohou vyzkoušet technologie podle svého zájmu.

## Requirements
1. Zanalyzujte a popište aktuální stav software pro organizaci týmů.
2. Implementujte webovou aplikaci podle zadání. Myslete na User Experience jak z pohledu zájemce o spolupráci, tak z pohledu správce týmu. Součástí by měla být i nějaká statistika ukazující, v jakém stavu jsou jednotlivé oblasti, např. kolik týmů hledá partnera pro předmět EAR.
3. Implementaci vyzkoušejte.
4. Řešení implementujte v technologii JakartaEE. Technologie na uživatelské rozhraní je na vůli studenta, musí ji odůvodnit.

## Resources
- [ ] [The Jakarta EE 8 Tutorial](https://eclipse-ee4j.github.io/jakartaee-tutorial/)
- [ ] [Jakarta EE Cookbook - Second Edition](https://www.packtpub.com/programming/jakarta-ee-cookbook-second-edition) files
- [ ] Patterns of Enterprise Application Architecture — Martin Fowler
