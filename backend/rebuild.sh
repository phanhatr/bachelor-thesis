#!/bin/bash
echo "****************************** Building Java"
mvn clean install

echo "****************************** Building Docker"
mkdir -p lib
curl -s https://jdbc.postgresql.org/download/postgresql-42.5.4.jar -o lib/postgresql.jar

docker build -t teamcollab-payara .

echo "****************************** Shutting down docker-compose"
docker-compose down
echo "****************************** Starting docker-compose"
docker-compose up -d  --remove-orphans

echo "****************************** Logs"
docker-compose logs -f
