import './ProjectItem.js'
import './Divider.js'
import { dev } from './config.js'

// Fetch All Projects
export async function fetchProjects () {
  const res = await fetch(dev + 'projects/all')
  const json = await res.json()

  const main = document.querySelector('main')

  json.forEach(project => {
    const projectItemEl = document.createElement('c-project-item')
    projectItemEl.project = project
    main.appendChild(projectItemEl)
  })
}

export async function fetchProjectById () {
  const queryString = window.location.search
  const urlParams = new URLSearchParams(queryString)
  const project = urlParams.get('project')

  const resProject = await fetch(dev + 'projects/' + project)
  const resMyRequestStatus = await fetch(
    dev + 'memberships/' + project + '/status'
  )

  const main = document.querySelector('main')
  const projectEl = document.createElement('c-project')

  projectEl.project = await resProject.json()
  if (resMyRequestStatus.statusText === 'OK') {
    projectEl.myProjectRequestStatus = await resMyRequestStatus.json()
  }

  main.appendChild(projectEl)
}

// Fetch Current User Info
export async function fetchPrivateUser () {
  const res = await fetch(dev + 'users/private')
  const json = await res.json()

  const nameBox = document.querySelector('.name-box')

  const userInfoEl = document.createElement('span')
  userInfoEl.textContent = json.firstName + ' ' + json.lastName
  nameBox.appendChild(userInfoEl)
}

// Fetch Project Details by Project ID
export async function fetchProjectDetails () {
  const queryString = window.location.search
  const urlParams = new URLSearchParams(queryString)
  const project = urlParams.get('project')

  const resProject = await fetch(dev + 'projects/' + project)
  const resMyRequestStatus = await fetch(
    dev + 'memberships/' + project + '/status'
  )

  const main = document.querySelector('main')
  const projectEl = document.createElement('c-project')

  projectEl.project = await resProject.json()
  if (resMyRequestStatus.statusText === 'OK') {
    projectEl.myProjectRequestStatus = await resMyRequestStatus.json()
  }

  main.appendChild(projectEl)
}


