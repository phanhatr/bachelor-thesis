import { fetchPrivateUser } from './fetchApi.js'
import { myFetch } from './FetchFunction.js'
import { dev } from './config.js'

window.addEventListener('load', () => {
  fetchPrivateUser()
  const signOutBtn = document.querySelector('.sign-out')

  const signOutRequest = async () => {
    try {
      await myFetch(dev + 'users/logout', 'POST')
      alert('User logged out')
      window.location.href = '/secured/home.html'
    } catch (err) {
      console.error(err)
      alert(err.response ? err.response.data.message : 'Something went wrong')
    }
  }

  signOutBtn.addEventListener('click', signOutRequest)
})
