import { fetchPrivateUser, fetchProjects } from './fetchApi.js'
import { myFetch } from './FetchFunction.js'
import './Logout.js'

const sortOptions = ['Oldest first', 'Newest first', 'Most members']

window.addEventListener('load', () => {
  fetchProjects()
})
