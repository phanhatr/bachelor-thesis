const template = document.createElement('template')
template.innerHTML = /*html*/ `
<style>
.position-background {
  display: flex;
  padding-bottom: 1rem;
  align-items: center;
  border-top-width: 1px;
}

.position-text{
  font-size: 0.875rem;
  line-height: 1.25rem;
  font-weight: 500;
  margin-left: 1rem;
}
</style>
<div class='position-background'>
  <svg viewBox="0 0 32 32" fill="none" width="2.5rem"
      xmlns="http://www.w3.org/2000/svg">
      <path d="M0 16C0 7.16344 7.16344 0 16 0C24.8366 0 32 7.16344 32 16C32 24.8366 24.8366 32 16 32C7.16344 32 0 24.8366 0 16Z" fill="#ffeeb8"/>
      <path d="M16 8.5L16.4472 7.60557C16.1657 7.46481 15.8343 7.46481 15.5528 7.60557L16 8.5ZM22.6667 11.8333H23.6667C23.6667 11.4546 23.4527 11.1083 23.1139 10.9389L22.6667 11.8333ZM9.33333 11.8333L8.88611 10.9389C8.54733 11.1083 8.33333 11.4546 8.33333 11.8333H9.33333ZM22.6667 20.1667L23.1139 21.0611C23.4527 20.8917 23.6667 20.5454 23.6667 20.1667H22.6667ZM16 23.5L15.5528 24.3944C15.8343 24.5352 16.1657 24.5352 16.4472 24.3944L16 23.5ZM9.33333 20.1667H8.33333C8.33333 20.5454 8.54733 20.8917 8.88611 21.0611L9.33333 20.1667ZM15.5528 9.39443L22.2194 12.7278L23.1139 10.9389L16.4472 7.60557L15.5528 9.39443ZM22.2194 10.9389L15.5528 14.2722L16.4472 16.0611L23.1139 12.7278L22.2194 10.9389ZM16.4472 14.2722L9.78054 10.9389L8.88611 12.7278L15.5528 16.0611L16.4472 14.2722ZM9.78054 12.7278L16.4472 9.39443L15.5528 7.60557L8.88611 10.9389L9.78054 12.7278ZM22.2194 19.2722L15.5528 22.6056L16.4472 24.3944L23.1139 21.0611L22.2194 19.2722ZM16.4472 22.6056L9.78054 19.2722L8.88611 21.0611L15.5528 24.3944L16.4472 22.6056ZM10.3333 20.1667V11.8333H8.33333V20.1667H10.3333ZM23.6667 20.1667V11.8333H21.6667V20.1667H23.6667ZM15 15.1667V23.5H17V15.1667H15Z" fill="#b88d00"/>
  </svg>
  <div class='position-text'></div>
</div>
`

class PositionItem extends HTMLElement {
  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
    this.shadowRoot.appendChild(template.content.cloneNode(true))
    this.shadowRoot.querySelector('.position-text').innerText =
      this.getAttribute('name')
  }
}

customElements.define('c-position-item', PositionItem)
