import './Tag.js'
import { icons } from './icons.js'

class ProjectItem extends HTMLElement {
  _project = []

  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
  }

  get project () {
    return this._project || []
  }

  set project (value) {
    this._project = value
    this.render()
  }

  connectedCallback () {}

  get style () {
    return /*css*/ ` 
        <style>
        .icon-wrap {
          padding-right: 2.5rem;
          padding-left: 1rem;
          width: 4.5rem;
          display: flex;
          align-items: center;
        }
        
        .project-title-wrap {
          color: black;
          font-size: 1.25rem;
          line-height: 1.75rem;
          font-weight: 700;
        }

        ul.positions-wanted {
          margin-block-start: 0 !important;
          padding-inline-start: 0 !important;
        }
        
        .positions-wanted {
          display: flex;
          margin-left: 0.2rem;
        }
        
        .current-members-wrap {
          display: flex;
          color: #4b5563;
          font-size: 14px;
          font-weight: 600;
          align-items: center;
          width: 6rem;
        }

        .member-count {
          min-width: 3.5rem;
        }
        
        .project-description {
          color: #6b7280;
          font-size: 0.875rem;
          line-height: 1.25rem;
          max-width: 56rem;
          text-overflow: ellipsis;
          white-space: nowrap;
          overflow: hidden;
        }
        
        .view-panel {
          padding-right: 2.5rem;
        }
        
        .view-wrap {
          display: flex;
          font-size: 0.875rem;
          line-height: 1.25rem;
          font-weight: 500;
          white-space: nowrap;
          position: relative;
          top: 50%;
          transform: translateY(-50%);
          color: #8900B6;
        }

        .left-project-item-wrap {
          display: flex;
        }

        .project-item {
          display: flex;
          justify-content: space-between;
          padding: 1.25rem;
          margin-bottom: 0.75rem;
          background-color: #ffffff;
          border-radius: 0.45rem;
          color: inherit;
          text-decoration: inherit;
          border: 0.12rem solid #e5e7eb;
        }
        
        .project-item:hover {
          background-color: #f3f4f6;
        }

        #people-icon {
          padding-right: 0.75rem;
        }

        #arrow-icon {
          padding-left: 0.75rem;
        }
        
        </style>
      `
  }

  get template () {
    const Icon = icons[this._project.category.categoryName]

    return /*html*/ `
        <a class='project-item' href="project.html?project=${
          this._project.projectId
        }">
          <div class="left-project-item-wrap">
            <div class='icon-wrap'>
              ${Icon}
            </div>
            <div>
              <div class='project-title-wrap'>
                <p>${this._project.name}</p>
              </div>
              <ul class='positions-wanted'>
              ${this._project.positionsWanted
                .map(
                  i =>
                    `<c-tag key="${i.positionsWantedId}" name="${i.positionsWantedName}"></c-tag>`
                )
                .join('')}
              </ul>
              <div class='current-members-wrap'>
              <svg viewBox="-6.48 -6.48 36.96 36.96" fill="none" xmlns="http://www.w3.org/2000/svg" stroke="#000000" stroke-width="0.36">
                <g id="SVGRepo_bgCarrier" stroke-width="0" transform="translate(0,0), scale(1)"/>
                <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round" stroke-width="0.096"/>
                <g id="SVGRepo_iconCarrier"> <path d="M15.8513 11.6882C16.0596 11.7921 16.372 11.7921 16.6845 11.7921C18.5591 11.7921 20.1213 10.2336 20.1213 8.36349C20.1213 6.49336 18.5591 4.93492 16.6845 4.93492C16.5803 4.93492 16.5803 4.93492 16.4762 4.93492C17.1011 5.76609 17.4135 6.80505 17.4135 7.94791C17.4135 9.29855 16.8928 10.6492 15.8513 11.6882Z" fill="#000000"/> <path d="M18.0383 13.1429H17.3093C18.6632 14.1818 19.4964 15.8442 19.4964 17.7143C19.4964 18.1299 19.3922 18.5455 19.2881 18.961C20.3295 18.7533 20.9544 18.5455 21.371 18.3377C21.7876 18.1299 21.9959 17.6104 21.9959 17.0909C22.1 14.9091 20.2254 13.1429 18.0383 13.1429Z" fill="#000000"/> <path d="M7.31142 11.7922C7.62386 11.7922 7.83215 11.7922 8.14458 11.6883C7.20728 10.6494 6.58241 9.4026 6.58241 7.84416C6.58241 6.7013 6.89484 5.66234 7.51971 4.83117C7.51971 4.83117 7.41557 4.83117 7.31142 4.83117C5.43681 4.83117 3.87463 6.38962 3.87463 8.25975C3.87463 10.1299 5.33267 11.7922 7.31142 11.7922Z" fill="#000000"/> <path d="M6.58238 13.1429H5.95751C3.77047 13.1429 2 14.9091 2 17.0909C2 17.6104 2.20829 18.1299 2.62487 18.3377C3.04145 18.5455 3.66632 18.8571 4.70777 18.961C4.60363 18.5455 4.49948 18.1299 4.49948 17.7143C4.49948 15.9481 5.2285 14.2857 6.58238 13.1429Z" fill="#000000"/> <path d="M9.60264 10.8571C10.2275 11.3766 11.0607 11.6883 11.998 11.6883C12.9353 11.6883 13.7684 11.3766 14.3933 10.8571C15.3306 10.1298 15.8513 9.09086 15.8513 7.84411C15.8513 6.90905 15.5389 5.97398 14.914 5.3506C14.185 4.51943 13.1436 3.99995 11.998 3.99995C10.8524 3.99995 9.70679 4.51943 9.08192 5.3506C8.45705 5.97398 8.04047 6.90905 8.04047 7.84411C8.04047 9.09086 8.66534 10.1298 9.60264 10.8571Z" fill="#000000"/> <path d="M14.0807 13.3506C13.8724 13.3506 13.6642 13.3506 13.4559 13.3506H10.3315C10.1232 13.3506 9.91493 13.3506 9.70664 13.3506C7.51959 13.6623 5.85327 15.5324 5.85327 17.7142C5.85327 18.3376 6.16571 18.8571 6.58229 19.0649C7.3113 19.4805 8.76933 20 11.7895 20C14.8097 20 16.2678 19.4805 16.9968 19.0649C17.5175 18.7532 17.7258 18.2337 17.7258 17.7142C18.0382 15.5324 16.2678 13.6623 14.0807 13.3506Z" fill="#000000"/> </g>
              </svg>
              <div class="member-count">
                ${this._project.memberCount} / ${this._project.maxMembers}
              </div>
              </div>
              <p class='project-description'>
                ${this._project.description}
              </p>
            </div>
          </div>
          <div class='view-panel'>
            <div class='view-wrap'>
              View Project
              <svg viewBox='0 0 20 10' fill='none' width='20' id="arrow-icon">
                <path d='M15 1L19 5M19 5L15 9M19 5L1 5' stroke='#8900B6' stroke-width='2' stroke-linecap='round' stroke-linejoin='round'/>
              </svg>
            </div>
        </div>
        </a>
      `
  }

  render () {
    this.shadowRoot.innerHTML = `${this.style}${this.template}`
  }
}

customElements.define('c-project-item', ProjectItem)
