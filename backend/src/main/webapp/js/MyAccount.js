import './Logout.js'
import { myFetch } from './FetchFunction.js'
import { dev } from './config.js'

const currentPasswordEl = document.querySelector('#current-password')
const newPasswordEl = document.querySelector('#new-password')
const confirmNewPasswordEl = document.querySelector('#confirm-new-password')

const form = document.querySelector('#change-password-form')

const checkCurrentPassword = () => {
  let valid = false
  const currentPassword = currentPasswordEl.value.trim()

  if (!isRequired(currentPassword)) {
    showError(currentPasswordEl, 'First name cannot be blank.')
  } else {
    showSuccess(currentPasswordEl)
    valid = true
  }
  return valid
}

const checkPassword = () => {
  let valid = false

  const password = newPasswordEl.value.trim()

  if (!isRequired(password)) {
    showError(newPasswordEl, 'Password cannot be blank.')
  } else if (!isPasswordSecure(password)) {
    showError(
      newPasswordEl,
      'Password must has at least 8 characters that include at least 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character in (!@#$%^&*)'
    )
  } else {
    showSuccess(newPasswordEl)
    valid = true
  }

  return valid
}

const checkConfirmPassword = () => {
  let valid = false
  // check confirm password
  const confirmPassword = confirmNewPasswordEl.value.trim()
  const password = newPasswordEl.value.trim()

  if (!isRequired(confirmPassword)) {
    showError(confirmNewPasswordEl, 'Please enter the password again')
  } else if (password !== confirmPassword) {
    showError(confirmNewPasswordEl, 'The password does not match')
  } else {
    showSuccess(confirmNewPasswordEl)
    valid = true
  }

  return valid
}

const isPasswordSecure = password => {
  const re = new RegExp(
    '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})'
  )
  return re.test(password)
}

const isRequired = value => value !== ''

const showError = (input, message) => {
  // get the form-field element
  const formField = input.parentElement
  // add the error class
  if (input.classList.contains('success')) input.classList.remove('success')
  input.classList.add('error')

  // show the error message
  const error = formField.querySelector('small')
  error.textContent = message
}

const showSuccess = input => {
  // get the form-field element
  const formField = input.parentElement

  // remove the error class
  if (input.classList.contains('error')) input.classList.remove('error')
  input.classList.add('success')

  // hide the error message
  const error = formField.querySelector('small')
  error.textContent = ''
}

form.addEventListener('submit', function (e) {
  // prevent the form from submitting
  e.preventDefault()

  // validate fields
  let isCurrentPasswordValid = checkCurrentPassword(),
    isNewPasswordValid = checkPassword(),
    isConfirmNewPasswordValid = checkConfirmPassword()

  let isFormValid =
    isCurrentPasswordValid && isNewPasswordValid && isConfirmNewPasswordValid

  // submit to the server if the form is valid
  if (isFormValid) {
    const sendRequest = async () => {
      try {
        await myFetch(dev + 'users/change/password', 'POST', {
          currentPassword: currentPasswordEl.value.trim(),
          newPassword: confirmNewPasswordEl.value.trim()
        })
        alert('Password has been successfully changed')
      } catch (err) {
        console.error(err)
        alert(err.response ? err.response.data.message : 'Something went wrong')
      }
    }
    sendRequest()
  }
})

const debounce = (fn, delay = 500) => {
  let timeoutId
  return (...args) => {
    // cancel the previous timer
    if (timeoutId) {
      clearTimeout(timeoutId)
    }
    // set up a new timer
    timeoutId = setTimeout(() => {
      fn.apply(null, args)
    }, delay)
  }
}

form.addEventListener(
  'input',
  debounce(function (e) {
    switch (e.target.id) {
      case 'current-password':
        checkCurrentPassword()
        break
      case 'new-password':
        checkPassword()
        break
      case 'confirm-new-password':
        checkConfirmPassword()
        break
    }
  })
)
