const template = document.createElement('template')
template.innerHTML = /*html*/ `
<style>
.position-background {
  display: flex;
  align-items: center;
  margin-bottom: 1.5rem;
}
.position-text{
  background-color: #dfd6f2;
  color: #3730A3;
  padding-top: 0.25rem;
  padding-bottom: 0.25rem; 
  padding-left: 0.75rem;
  padding-right: 0.75rem; 
  vertical-align: middle; 
  border-radius: 70px;
  font-weight: 500;
  margin-left: 1rem; 
  font-size: 14px;
}
.name-text {
  padding-top: 0.25rem;
  padding-bottom: 0.25rem; 
}
.email-text {
  display: flex;
  align-items: center;
  color: #6f1296;
  margin-top: 0.4rem;
}

.col{
  flex-direction: column;
}
.row{
  display: flex;
}
svg{
  width: 50px;
  height: 50px;
  margin-right: 1rem;
}
</style>
<div class='position-background'>
  <svg viewBox="0 0 40 40" fill="none">
      <g clip-path="url(#clip0)">
          <path d="M0 20C0 8.95431 8.95431 0 20 0C31.0457 0 40 8.95431 40 20C40 31.0457 31.0457 40 20 40C8.95431 40 0 31.0457 0 20Z" fill="#dfd6f2"/>
          <path d="M40 34.9902V40.0018H0V35.0085C2.32658 31.8993 5.34651 29.3757 8.81965 27.6386C12.2928 25.9014 16.1233 24.9986 20.0067 25.0018C28.18 25.0018 35.44 28.9252 40 34.9902ZM26.67 15.0002C26.67 16.7683 25.9676 18.464 24.7174 19.7142C23.4671 20.9644 21.7714 21.6668 20.0033 21.6668C18.2352 21.6668 16.5395 20.9644 15.2893 19.7142C14.039 18.464 13.3367 16.7683 13.3367 15.0002C13.3367 13.2321 14.039 11.5364 15.2893 10.2861C16.5395 9.03587 18.2352 8.3335 20.0033 8.3335C21.7714 8.3335 23.4671 9.03587 24.7174 10.2861C25.9676 11.5364 26.67 13.2321 26.67 15.0002Z" fill="#716389"/>
      </g>
      <defs>
          <clipPath id="clip0">
              <path d="M0 20C0 8.95431 8.95431 0 20 0C31.0457 0 40 8.95431 40 20C40 31.0457 31.0457 40 20 40C8.95431 40 0 31.0457 0 20Z" fill="white"/>
          </clipPath>
      </defs>
  </svg>
  <div class="col">
    <div class="row">
      <div class='name-text'></div>
      <div class='position-text'></div>
    </div>
    <div class='email-text'></div>
  </div>
</div>
`

class MemberItem extends HTMLElement {
  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
    this.shadowRoot.appendChild(template.content.cloneNode(true))
    this.shadowRoot.querySelector('.name-text').innerText =
      this.getAttribute('first-name') + ' ' + this.getAttribute('last-name')
    this.shadowRoot.querySelector('.position-text').innerText =
      this.getAttribute('position')
    this.shadowRoot.querySelector('.email-text').innerText =
      this.getAttribute('email')
  }
}

customElements.define('c-member-item', MemberItem)
