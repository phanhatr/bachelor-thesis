import { myFetch } from './FetchFunction.js'
import './JoinRequestItem.js'
import { dev } from './config.js'

class CreatorView extends HTMLElement {
  _projectJoinRequests = []
  _projectMemberships = []

  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
  }

  _project = []

  get project () {
    return this._project || []
  }

  set project (value) {
    this._project = value
    this._projectJoinRequests = this._project.joinRequests
    this._projectMemberships = this._project.memberships
    this.render()
  }

  get style () {
    return /*css*/ ` 
        <style>
        @import url('https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap');

        * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Inter', sans-serif;
      }
      
      body {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        color: #222;
        position: relative;
        min-height: 100vh;
        background-color: #b3e6f4;
      }
      
      .modal, .modal2 {
        display: flex;
        flex-direction: column;
        gap: 1rem;
        padding: 1.5rem;
        position: absolute;
        z-index: 2;
        top: 30%;
        background-color: white;
        border: 1px solid #ddd;
        border-radius: 15px;
        left: 50%;
        margin-left: -340px;
        width: 740px;
      }
      
      .modal .flex, .modal2 .flex {
        display: flex;
        align-items: center;
        justify-content: space-between;
      }
      
      .modal input {
        padding: 0.7rem 1rem;
        border: 1px solid #ddd;
        border-radius: 5px;
        font-size: 0.9em;
      }
      
      .modal p {
        font-size: 0.9rem;
        color: #777;
        margin: 0.4rem 0 0.2rem;
      }

      .view-buttons {
        text-align: right;
      }
      
      .btn {
        background-color: #121212;
        background-image: none;
        border: 1px solid #000;
        border-radius: 4px;
        box-shadow: #fff 4px 4px 0 0,#000 4px 4px 0 1px;
        box-sizing: border-box;
        color: #fff;
        cursor: pointer;
        display: inline-block;
        font-family: 'Inter', sans-serif;
        font-size: 14px;
        font-weight: bold;
        line-height: 20px;
        margin: 0 5px 10px 0;
        overflow: visible;
        padding: 12px 40px;
        text-align: center;
        text-transform: none;
        touch-action: manipulation;
        user-select: none;
        -webkit-user-select: none;
        vertical-align: middle;
        white-space: nowrap;
        }

      .no-requests {
        padding: 0.6rem;
        margin-left: 0.5rem;
        background-color: #dfd6f2;
        color: #716389;
        font-weight: 700;
        border-radius: 0.375rem;
        text-align: center;
      }

      .btn-view {
        background-color: #716389;
      }

      .btn-leave {
        display: inline-block;
        padding: 0.8rem 1.4rem;
        font-weight: 700;
        background-color: #d4d0db;
        color: #121212;
        border-radius: 5px;
        text-align: center;
      }

      .btn-close {
        margin: 0 auto;
        margin-top: 30px;
        border: 0;
        padding: 0;
        background: #716389;
        border-radius: 50%;
        width: 40px;
        height: 40px;
        display: flex;
        flex-flow: column nowrap;
        justify-content: center;
        align-items: center;
        cursor: pointer;
        transition: all 150ms;
     }
      .btn-close .icon-cross {
        margin: 0;
        padding: 0;
        border: 0;
        background: none;
        position: relative;
        width: 20px;
        height: 20px;
     }
      .btn-close .icon-cross:before, .btn-close .icon-cross:after {
        content: '';
        position: absolute;
        top: 8.5px;
        left: 0;
        right: 0;
        height: 3px;
        background: #fff;
        border-radius: 3px;
     }
      .btn-close .icon-cross:before {
        transform: rotate(45deg);
     }
      .btn-close .icon-cross:after {
        transform: rotate(-45deg);
     }
      .btn-close .icon-cross span {
        display: block;
     }
      .btn-close:hover, .btn-close:focus {
        transform: rotateZ(90deg);
        background: black;
     }     
      
      .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.5);
        backdrop-filter: blur(3px);
        z-index: 1;
      }
      
      textarea {
      padding-top: 0.5rem;
      padding-bottom: 0.5rem; 
      padding-left: 1rem;
      padding-right: 1rem; 
      width: 100%; 
      border-radius: 0.375rem; 
      border-width: 1px; 
      border-color: #D1D5DB; 
      box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.05); 
      }
      
      .title {
      margin-bottom: 1.5rem; 
      font-size: 1.25rem;
      line-height: 1.75rem; 
      font-weight: 500; 
      text-align: center; 
      }
      
      .buttons{
      display: flex; 
      margin-left: 0.5rem; 
      }
      .message{
      display: block; 
      margin-bottom: 0.25rem; 
      font-weight: 500; 
      }
      
      .write {
      margin-bottom: 0.75rem; 
      color: #6B7280; 
      font-size: 0.875rem;
      line-height: 1.25rem; 
      font-weight: 400; 
      width: 100%; 
      }
      
      .hidden {
        display: none;
      }
      
      .project-name {
          color: #6f1296;
          font-weight: 500;
      }

      .visually-hidden {
        position: absolute !important;
        clip: rect(1px, 1px, 1px, 1px);
        padding: 0 !important;
        border: 0 !important;
        height: 1px !important;
        width: 1px !important;
        overflow: hidden;
      }
      
        </style>
      `
  }

  callFunctions () {
    const modal = this.shadowRoot.querySelector('.modal')
    const modal2 = this.shadowRoot.querySelector('.modal2')
    const overlay = this.shadowRoot.querySelector('.overlay')
    const closeModalBtn = this.shadowRoot.querySelector('.btn-close')
    const closeModalBtn2 = this.shadowRoot.querySelector('.btn-close2')

    const closeModal = function () {
      modal.classList.add('hidden')
      modal2.classList.add('hidden')
      overlay.classList.add('hidden')
    }

    // Close the modal when the close button and overlay is clicked

    closeModalBtn.addEventListener('click', closeModal)
    closeModalBtn2.addEventListener('click', closeModal)
    overlay.addEventListener('click', closeModal)

    // Close modal when the Esc key is pressed

    this.shadowRoot.addEventListener('keydown', function (e) {
      if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
        closeModal()
      }
    })

    // open modal function
    const openModal = function () {
      modal.classList.remove('hidden')
      overlay.classList.remove('hidden')
    }

    const openModal2 = function () {
      modal2.classList.remove('hidden')
      overlay.classList.remove('hidden')
    }

    const leaveProject = async () => {
      let isExecuted = confirm('Are you sure to execute this action?')
      if (isExecuted) {
        try {
          await myFetch(
            dev + 'memberships/' + this._project.projectId + '/leave',
            'DELETE',
            {}
          )
          alert('You left the project')
          window.location.replace('/secured/home.html')
        } catch (err) {
          console.error(err)
          alert(
            err.response ? err.response.data.message : 'Something went wrong'
          )
        }
      }
    }

    const leaveBtn = this.shadowRoot.querySelector('.btn-leave')
    leaveBtn.addEventListener('click', leaveProject)

    // Request to join if no request exists
    if (this.shadowRoot.querySelector('.btn-open') != null) {
      const openModalBtn = this.shadowRoot.querySelector('.btn-open')
      openModalBtn.addEventListener('click', openModal)
    }

    if (this.shadowRoot.querySelector('.btn-edit') != null) {
      const editModalBtn = this.shadowRoot.querySelector('.btn-edit')
      editModalBtn.addEventListener('click', openModal2)
    }

    const joinRequestsList = this.shadowRoot.querySelector(
      '.join-requests-list'
    )

    if (this._projectJoinRequests.length > 0) {
      this._projectJoinRequests.forEach(createRequestItem)

      function createRequestItem (item) {
        const requestItemEl = document.createElement('c-join-request-item')
        requestItemEl.joinRequest = item
        joinRequestsList.appendChild(requestItemEl)
      }
    } else {
      const requestsPlaceholderEl = document.createElement('div')
      requestsPlaceholderEl.className = 'no-requests'
      requestsPlaceholderEl.innerHTML =
        'There are no requests to join the project'
      joinRequestsList.appendChild(requestsPlaceholderEl)
    }
  }

  render () {
    this.shadowRoot.innerHTML = /*html*/ `
    ${this.style}
    <section class="modal hidden">
      <div class="requests-title">
        <h3>Join requests</h3>
      </div>
      <div class="join-requests-list"></div>
      <button class="btn-close" role="button">
        <span class="icon-cross"></span>
        <span class="visually-hidden">Close</span>
      </button>
    </section>

    <section class="modal2 hidden">
      <div class="requests-title">
        <h3>Edit project</h3>
      </div>
      <form action="" id="new-project-form" method="POST" name="project-form">
        <div class="buttons">
          <button class="btn btn-cancel btn-close2" role="button">
            Cancel
          </button>
          <button class="btn btn-create" id="submit" type="submit">
            Save changes
          </button>
        </div>
        <fieldset>
          <label class="field-label" for="project-title" id="name-label">
            Project title *
          </label>
          <input
            class=""
            id="project-title"
            name="project-title"
            placeholder="Enter project title (required)"
            required
            type="text"
            value="${this._project.name}"
            />
        </fieldset>
        <fieldset>
          <label class="field-label" for="max-members" id="number-label">
            Number of wanted members *
          </label>
          <input
            class=""
            id="max-members"
            max="999"
            min="0"
            name="max-members"
            placeholder="Enter number of wanted members (required)"
            required
            type="number"
            value="${this._project.maxMembers}"
          />
        </fieldset>
        <fieldset>
          <div class="field-label labels">Category *</div>
          <div id="category-radios"></div>
        </fieldset>
        <fieldset>
          <label class="field-label" for="project-description">
            Project description *
          </label>
          <textarea
            id="project-description"
            maxlength="666"
            placeholder="Write few sentences about the project. (required)"
            rows="7"
          >${this._project.description}</textarea>
        </fieldset>
        <fieldset>
          <label class="field-label wanted-positions" for="wanted-positions">
            Wanted positions *
            <select
              class="wanted-positions-select"
              data-placeholder="Wanted positions"
              id="wanted-positions"
              multiple
            ></select>
          </label>
        </fieldset>
      </form>
      <button class="btn-close2" role="button">
        <span class="icon-cross"></span>
        <span class="visually-hidden">Close</span>
      </button>
    </section>

    <div class="overlay hidden"></div>
    <div class="view-buttons">
      <button class="btn btn-leave">Leave project</button>
      <button class="btn btn-open btn-view">View requests</button>
      <button class="btn btn-edit btn-view">Edit</button>
    </div>
    `
    this.callFunctions()
  }

  render2 () {
    this.shadowRoot.innerHTML = /*html*/ `
    ${this.style}
    <div>
      <a href="home.html" class="link-home">
        <div class="arrow-wrap">
          <svg
            viewBox="0 0 20 10"
            fill="none"
            width="20"
            xmlns="http://www.w3.org/200 0/svg"
          >
            <path
              d="M5 9L1 5M1 5L5 1M1 5L19 5"
              stroke="#6B7280"
              stroke-width="2"
              stroke-linecap="round"
              stroke-linejoin="round"
            />
          </svg>
        </div>
        back to Projects
      </a>
    </div>
    <div class="bottom-panel">
      <form action="" id="new-project-form" method="POST" name="project-form">
        <div class="buttons">
          <a class="btn btn-cancel" href="../secured/home.html"> Cancel </a>
          <button class="btn btn-create" id="submit" type="submit">
            Publish project
          </button>
        </div>
        <fieldset>
          <label class="field-label" for="project-title" id="name-label">
            Project title *
          </label>
          <input
            class=""
            id="project-title"
            name="project-title"
            placeholder="Enter project title (required)"
            required
            type="text"
          >{this._project.name}</input>
        </fieldset>
        <fieldset>
          <label class="field-label" for="max-members" id="number-label">
            Number of wanted members *
          </label>
          <input
            class=""
            id="max-members"
            max="999"
            min="0"
            name="max-members"
            placeholder="Enter number of wanted members (required)"
            required
            type="number"
          />
        </fieldset>
        <fieldset>
          <div class="field-label labels">Category *</div>
          <div id="category-radios"></div>
        </fieldset>
        <fieldset>
          <label class="field-label" for="project-description">
            Project description *
          </label>
          <textarea
            id="project-description"
            max-length="666"
            placeholder="Write few sentences about the project."
            rows="7"
          ></textarea>
        </fieldset>
        <fieldset>
          <label class="field-label wanted-positions" for="wanted-positions">
            Wanted positions *
            <select
              class="wanted-positions-select"
              data-placeholder="Wanted positions"
              id="wanted-positions"
              multiple
            ></select>
          </label>
        </fieldset>
      </form>
    </div>
    `
  }
}

customElements.define('c-creator-view', CreatorView)
