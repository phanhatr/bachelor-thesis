import './Logout.js'
import { dev } from './config.js'

let projects,
  categories,
  positions = []

export async function fetchProjects () {
  //   const projectsRes = await fetch(dev + 'projects/myProjects')
  //   projects = await projectsRes.json()
  //   const categoriesRes = await fetch(dev + 'categories')
  //   categories = await categoriesRes.json()
  //   const positionsRes = await fetch(dev + 'positions')
  //   positions = await positionsRes.json()

  try {
    const projectsRes = fetch(dev + 'projects/all')
    const categoriesRes = fetch(dev + 'categories')
    const positionsRes = fetch(dev + 'positions')

    const [projects, categories, positions] = await Promise.all([
      projectsRes,
      categoriesRes,
      positionsRes
    ]).then(responses =>
      Promise.all(responses.map(response => response.json()))
    )

    // Perform further actions with the fetched data
    const categoryCount = countProjectsByCategory(projects, categories)
    const positionCount = countProjectsByPosition(projects, positions)

    const membersCount = countMembersByCategory(projects)

    const mainElement = document.querySelector('main')

    // Create a table element
    const tableElement = document.createElement('table')

    // Create the table header row
    const headerRow = tableElement.insertRow()
    const headerCell1 = headerRow.insertCell()
    headerCell1.textContent = 'Position'
    const headerCell2 = headerRow.insertCell()
    headerCell2.textContent = 'Projects total'

    // Populate the table with positionCount data
    for (const position in positionCount) {
      const count = positionCount[position]

      const row = tableElement.insertRow()
      const positionCell = row.insertCell()
      positionCell.textContent = position
      const countCell = row.insertCell()
      countCell.textContent = count
    }

    // Create an empty row as a separator
    tableElement.insertRow()

    const tableElement2 = document.createElement('table')

    // Create the table header row for categoryCount
    const categoryHeaderRow = tableElement2.insertRow()
    const categoryHeaderCell1 = categoryHeaderRow.insertCell()
    categoryHeaderCell1.textContent = 'Category'
    const categoryHeaderCell2 = categoryHeaderRow.insertCell()
    categoryHeaderCell2.textContent = 'Projects total'
    const categoryHeaderCell3 = categoryHeaderRow.insertCell()
    categoryHeaderCell3.textContent = 'Current members'

    // Populate the table with categoryCount data
    for (const category in categoryCount) {
      const count = categoryCount[category]

      const row = tableElement2.insertRow()
      const categoryCell = row.insertCell()
      categoryCell.textContent = category
      const countCell = row.insertCell()
      countCell.textContent = count
    }

    // Append the table to the <main> element
    mainElement.appendChild(tableElement)
    mainElement.appendChild(tableElement2)

    // Additional code here...
  } catch (error) {
    // Handle error if any of the fetch requests fail
    console.log('Error:', error)
  }
}

window.addEventListener('load', () => {
  fetchProjects()
})

function countProjectsByCategory (projects, categories) {
  const categoryCount = {}

  projects.forEach(project => {
    const projectCategory = project.category

    categories.forEach(category => {
      if (category.categoryId === projectCategory.categoryId) {
        const categoryName = category.categoryName

        if (categoryCount.hasOwnProperty(categoryName)) {
          categoryCount[categoryName]++
        } else {
          categoryCount[categoryName] = 1
        }
      }
    })
  })

  return categoryCount
}

function countProjectsByPosition (projects, positions) {
  const positionCount = {}

  projects.forEach(project => {
    const projectPositions = project.positionsWanted

    projectPositions.forEach(projectPosition => {
      const positionId = projectPosition.positionsWantedId
      const positionName = positions.find(
        position => position.positionsWantedId === positionId
      ).positionsWantedName

      if (positionCount.hasOwnProperty(positionName)) {
        positionCount[positionName]++
      } else {
        positionCount[positionName] = 1
      }
    })
  })

  return positionCount
}

function countMembersByCategory (projects) {
  const categoryMemberCount = {}

  projects.forEach(project => {
    const category = project.category
    const memberCount = project.memberCount

    if (categoryMemberCount.hasOwnProperty(category)) {
      categoryMemberCount[category] += memberCount
    } else {
      categoryMemberCount[category] = memberCount
    }
  })

  return categoryMemberCount
}
