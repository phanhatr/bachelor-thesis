class Hello extends HTMLElement {
  _project = []

  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
  }

  get project () {
    return this._project || []
  }

  set project (value) {
    this._project = value
    this.render()
  }

  connectedCallback () {}

  get style () {
    return /*css*/ ` 
      `
  }

  get template () {
    return /*html*/ `
    <p>I am rendering this</p>
      `
  }

  render () {
    this.shadowRoot.innerHTML = `${this.style}${this.template}`
  }
}

customElements.define('c-hello', Hello)
