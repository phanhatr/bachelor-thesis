import './Logout.js'
import {dev} from './config.js'

export async function fetchProjects() {
    const res = await fetch(dev + 'projects/myProjects')
    const json = await res.json()

    const main = document.querySelector('main')

    const dividerEl = document.createElement('c-divider')
    main.appendChild(dividerEl)

    json.forEach(project => {
        const projectItemEl = document.createElement('c-project-item')
        projectItemEl.project = project
        main.appendChild(projectItemEl)
    })
}

window.addEventListener('load', () => {
    fetchProjects()
})
