import './Tag.js'
import './ViewList.js'
import './PositionItem.js'
import './MemberItem.js'
import { icons } from './icons.js'

class Project extends HTMLElement {
  _project = []

  _members = []
  _myProjectRequestStatus = []
  _projectRequests = []

  _isCreator = false
  _isMember = false

  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
  }

  get project () {
    return this._project || []
  }

  set project (value) {
    this._project = value
    this._isCreator = this._project.creator
    this._isMember = this._project.member
    this.render()
  }

  get myProjectRequestStatus () {
    return this._myProjectRequestStatus || []
  }

  set myProjectRequestStatus (value) {
    this._myProjectRequestStatus = value
    this.render()
  }

  get style () {
    return /*css*/ ` 
        <style>
        .link-home {
          display: flex; 
          color: #6B7280; 
          font-size: 0.875rem;
          line-height: 1.25rem; 
          font-weight: 500; 
          white-space: nowrap; 
          width: 9rem;
          text-decoration:  none;
        }
        .arrow-wrap {
          margin-right: 1rem;
        }
        .head-panel {
          display: flex;
          align-items: flex-start;
          justify-content: space-between;
          margin: 40px 10px 0 10px;
        }

        .left-head-panel {
          display: flex;
        }
        
        .icon-wrap-large {
          padding-right: 1.5rem;
          width: 4.5rem;
          align-items: center;
          display: flex;
          justify-content: flex-end;
        }
        .title-wrap {
          display: flex;
          flex-direction: column;
        }
        .creator-wrap {
          margin-top: 0.5rem;
          color: #9ca3af;
          font-weight: 500;
        }
        .project-creator {
          color: #4B5563; 
        }
        .bottom-panel {
          padding-top: 2rem;
          border-top-width: 1px;
          margin: 0 0 0 10px;
        }
        
        .description-wrap {
          margin-bottom: 2rem;
          color: #6b7280;
          font-size: 0.875rem;
          line-height: 1.25rem;
          max-width: 80rem;
        }
        
        .position-title {
          display: flex;
          padding-top: 1.25rem;
          padding-bottom: 1.25rem;
          align-items: center;
          border-top-width: 1px;
        }
        
        .position-count-icon {
          padding: 0.2rem 0.6rem 0.2rem 0.6rem;
          margin-left: 0.5rem;
          background-color: #dfd6f2;
          color: #716389;
          font-weight: 700;
          border-radius: 0.375rem;
        }
        .position-list-wrap {
          padding-left: 1rem;
          padding-right: 1rem;
        }

        </style>
      `
  }

  setProjectData () {
    const viewListEl = this.shadowRoot.querySelector('c-view-list')
    viewListEl.project = this._project
    viewListEl.myProjectRequestStatus = this._myProjectRequestStatus
    viewListEl.isCreator = this._isCreator
    viewListEl.isMember = this._isMember
  }

  render () {
    const Icon = icons[this._project.category.categoryName]
    let dateTimeCreated = new Date(this._project.dateTimeCreated)
    dateTimeCreated = dateTimeCreated.toLocaleDateString()

    this.shadowRoot.innerHTML = /*html*/ `
    ${this.style}
    <div>
    <a href="home.html" class="link-home">
    <div class="arrow-wrap">
      <svg viewBox="0 0 20 10" fill="none" width='20' xmlns="http://www.w3.org/200 0/svg">
      <path d="M5 9L1 5M1 5L5 1M1 5L19 5" stroke="#6B7280" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
    </div>
      back to Projects
    </a>
    <div class="head-panel">
      <div class="left-head-panel">
        <div class="icon-wrap-large">
          ${Icon}
        </div>
        <div class="title-wrap">
          <h1>${this._project.name}</h1>
          <div class="creator-wrap">
          Created ${dateTimeCreated} by
            <span class="project-creator">${this._project.projectCreator}</span>
          </div>
        </div>
      </div>
      <div class="right-head-panel">
        <c-view-list></c-view-list>
      </div>
    </div>
    </div>
    <div class="bottom-panel">
    <section class="description-wrap">${this._project.description}</section>
    <section>
      <div class="position-title">
        <h2>Positions wanted</h2>
        <div class="position-count-icon">${
          this._project.positionsWanted.length
        }</div>
      </div>
      <div class="position-list-wrap">
      ${this._project.positionsWanted
        .map(
          i =>
            `<c-position-item key="${i.positionsWantedId}" name="${i.positionsWantedName}"></c-position-item>`
        )
        .join('')}
      </div>
      <div class="member-list-wrap">
        ${
          this._isCreator || this._isMember
            ? `<div class="position-title">
                <h2>Members</h2>
            <div class="position-count-icon">${this._project.memberCount}</div>
          </div>
          ${this._project.memberships
            .map(
              i =>
                `<c-member-item email="${i.email}" first-name="${i.firstName}"
                 last-name="${i.lastName}" position="${i.position}"></c-member-item>`
            )
            .join('')}`
            : ``
        }
      </div>
    </section>
    </div>
    `
    this.setProjectData()
  }

  render () {
    const Icon = icons[this._project.category.categoryName]
    let dateTimeCreated = new Date(this._project.dateTimeCreated)
    dateTimeCreated = dateTimeCreated.toLocaleDateString()

    this.shadowRoot.innerHTML = /*html*/ `
    ${this.style}
    <div>
    <a href="home.html" class="link-home">
    <div class="arrow-wrap">
      <svg viewBox="0 0 20 10" fill="none" width='20' xmlns="http://www.w3.org/200 0/svg">
      <path d="M5 9L1 5M1 5L5 1M1 5L19 5" stroke="#6B7280" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
    </div>
      back to Projects
    </a>
    <div class="head-panel">
      <div class="left-head-panel">
        <div class="icon-wrap-large">
          ${Icon}
        </div>
        <div class="title-wrap">
          <h1>${this._project.name}</h1>
          <div class="creator-wrap">
          Created ${dateTimeCreated} by
            <span class="project-creator">${this._project.projectCreator}</span>
          </div>
        </div>
      </div>
      <div class="right-head-panel">
        <c-view-list></c-view-list>
      </div>
    </div>
    </div>
    <div class="bottom-panel">
    <section class="description-wrap">${this._project.description}</section>
    <section>
      <div class="position-title">
        <h2>Positions wanted</h2>
        <div class="position-count-icon">${
          this._project.positionsWanted.length
        }</div>
      </div>
      <div class="position-list-wrap">
      ${this._project.positionsWanted
        .map(
          i =>
            `<c-position-item key="${i.positionsWantedId}" name="${i.positionsWantedName}"></c-position-item>`
        )
        .join('')}
      </div>
      <div class="member-list-wrap">
        ${
          this._isCreator || this._isMember
            ? `<div class="position-title">
                <h2>Members</h2>
            <div class="position-count-icon">${this._project.memberCount}</div>
          </div>
          ${this._project.memberships
            .map(
              i =>
                `<c-member-item email="${i.email}" first-name="${i.firstName}"
                 last-name="${i.lastName}" position="${i.position}"></c-member-item>`
            )
            .join('')}`
            : ``
        }
      </div>
    </section>
    </div>
    `
    this.setProjectData()
  }
}

customElements.define('c-project', Project)
