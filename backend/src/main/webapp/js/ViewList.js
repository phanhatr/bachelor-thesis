import './NonMemberView.js'
import './MemberView.js'
import './CreatorView.js'

class ViewList extends HTMLElement {
  _project = []
  _myProjectRequestStatus = []

  _isCreator = false
  _isMember = false

  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
  }

  set project (value) {
    this._project = value
    this._isCreator = this._project.creator
    this._isMember = this._project.member
    this.render()
  }

  get project () {
    return this._project || []
  }

  set myProjectRequestStatus (value) {
    this._myProjectRequestStatus = value
    this.render()
  }

  get myProjectRequestStatus () {
    return this._myProjectRequestStatus || []
  }
  get style () {
    return /*css*/ `
        <style>
        </style>
      `
  }

  get template () {
    return /*html*/ `
    <c-project-item></c-project-item>
      `
  }

  render () {
    this.shadowRoot.innerHTML = `

    ${this.style}
    
    ${
      this._isCreator
        ? `<c-creator-view></c-creator-view>`
        : this._isMember
        ? `<c-member-view></c-member-view>`
        : `<c-non-member-view></c-non-member-view>`
    }
    `
    this.setProjectData()
  }

  setProjectData () {
    if (this.shadowRoot.querySelector('c-non-member-view') != null) {
      const nonMemberViewEL = this.shadowRoot.querySelector('c-non-member-view')
      nonMemberViewEL.project = this._project
      nonMemberViewEL.myProjectRequestStatus = this._myProjectRequestStatus
    } else {
    }

    if (this.shadowRoot.querySelector('c-creator-view') != null) {
      const creatorViewEL = this.shadowRoot.querySelector('c-creator-view')
      creatorViewEL.project = this._project
    } else {
    }

    if (this.shadowRoot.querySelector('c-member-view') != null) {
      const memberViewEL = this.shadowRoot.querySelector('c-member-view')
      memberViewEL.project = this._project
    } else {
    }
  }
}
customElements.define('c-view-list', ViewList)
