const template = document.createElement('template')
template.innerHTML = /*html*/ `
<style>
.tag-text{
font-size: 0.75rem;
line-height: 1rem; 
font-weight: 500; 
}
.tag-background{
  background-color: #dfd6f2;
  color: #3730A3;
  padding-top: 0.25rem;
  padding-bottom: 0.25rem; 
  padding-left: 0.75rem;
  padding-right: 0.75rem; 
  vertical-align: middle; 
  border-radius: 70px;
  font-weight: 700;
  margin-right: 0.25rem; 
  }
</style>
<div class='tag-background'>
  <div class='tag-text'></div>
</div>
`

class Tag extends HTMLElement {
  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
    this.shadowRoot.appendChild(template.content.cloneNode(true))
    this.shadowRoot.querySelector('.tag-text').innerText =
      this.getAttribute('name')
  }
}

customElements.define('c-tag', Tag)
