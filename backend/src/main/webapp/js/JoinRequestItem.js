import {myFetch} from './FetchFunction.js'
import {dev} from './config.js'

class JoinRequestItem extends HTMLElement {
    constructor() {
        super()

        this.attachShadow({mode: 'open'})
    }

    _joinRequest = []

    get joinRequest() {
        return this._joinRequest || []
    }

    set joinRequest(value) {
        this._joinRequest = value
        this.render()
    }

    get style() {
        return /*css*/ ` 
        <style>
        .position-background {
          display: flex;
          align-items: center;
          padding: 10px 10px 15px 15px;
          border: 1px solid #000;
          border-radius: 4px;
          margin-bottom: 0.5rem;
        }
        .position-text{
          background-color: #dfd6f2;
          color: #3730A3;
          padding: 0.25rem 0.75rem; 
          vertical-align: middle; 
          border-radius: 70px;
          font-weight: 500;
          margin-left: 1rem; 
          font-size: 14px;
        }
        .name-text {
          padding-top: 0.25rem;
          padding-bottom: 0.25rem; 
          min-width: 450px;
        }
        .email-text {
          display: flex;
          align-items: center;
          color: #6f1296;
          margin-top: 0.4rem;
        }
        .btn-request {
          background-color: #121212;
          background-image: none;
          border: 1px solid #000;
          border-radius: 4px;
          box-shadow: #fff 4px 4px 0 0, #000 4px 4px 0 1px;
          box-sizing: border-box;
          color: black;
          cursor: pointer;
          display: inline-block;
          font-size: 14px;
          font-weight: bold;
          line-height: 20px;
          margin: 0 11px 22px 0;
          overflow: visible;
          padding: 7px 10px;
          text-align: center;
          text-transform: none;
          touch-action: manipulation;
          user-select: none;
          -webkit-user-select: none;
          vertical-align: middle;
          white-space: nowrap;
        }
        .view-buttons{
          display: flex;
        }
        .btn-accept{
          background-color: #c3e6c1;
        }

        .btn-reject{
          background-color: #f1a2a2;
        }

        .request-rejected {
          padding: 0.6rem;
          margin-left: 0.5rem;
          color: #9c111f;
          font-weight: 700;
          border-radius: 0.375rem;
        }

        .btn-undo {
          background-color: #716389;
          color: #fff;
        }
        
        .col{
          flex-direction: column;
        }
        .row{
          display: flex;
        }
        svg{
          width: 80px;
          height: 80px;
          margin-right: 1rem;
        }
        </style>
      `
    }

    get template() {
        return /*html*/ `
    <div class='position-background'>
      <svg viewBox="0 0 40 40" fill="none">
        <g clip-path="url(#clip0)">
            <path d="M0 20C0 8.95431 8.95431 0 20 0C31.0457 0 40 8.95431 40 20C40 31.0457 31.0457 40 20 40C8.95431 40 0 31.0457 0 20Z" fill="#dfd6f2"/>
            <path d="M40 34.9902V40.0018H0V35.0085C2.32658 31.8993 5.34651 29.3757 8.81965 27.6386C12.2928 25.9014 16.1233 24.9986 20.0067 25.0018C28.18 25.0018 35.44 28.9252 40 34.9902ZM26.67 15.0002C26.67 16.7683 25.9676 18.464 24.7174 19.7142C23.4671 20.9644 21.7714 21.6668 20.0033 21.6668C18.2352 21.6668 16.5395 20.9644 15.2893 19.7142C14.039 18.464 13.3367 16.7683 13.3367 15.0002C13.3367 13.2321 14.039 11.5364 15.2893 10.2861C16.5395 9.03587 18.2352 8.3335 20.0033 8.3335C21.7714 8.3335 23.4671 9.03587 24.7174 10.2861C25.9676 11.5364 26.67 13.2321 26.67 15.0002Z" fill="#716389"/>
        </g>
        <defs>
            <clipPath id="clip0">
                <path d="M0 20C0 8.95431 8.95431 0 20 0C31.0457 0 40 8.95431 40 20C40 31.0457 31.0457 40 20 40C8.95431 40 0 31.0457 0 20Z" fill="white"/>
            </clipPath>
        </defs>
      </svg>
      <div class="col">
      <div class='email-text'>${this._joinRequest.user.firstName + ' '}${
            this._joinRequest.user.lastName
        }</div>
        <div class="row">
          <div class='name-text'>${this._joinRequest.message}</div>
          <div class="view-buttons">
          ${
            this._joinRequest.status === 'PENDING'
                ? `<button class="btn-request btn-accept">Accept</button>
              <button class="btn-request btn-reject">Reject</button>`
                : `<div class="request-rejected">Rejected</div>
              <button class="btn-request btn-undo">Undo</button>`
        }
          </div>
        </div>
      </div>
    </div>
      `
    }

    connectedCallback() {
    }

    callFunctions() {
        const acceptRequest = async () => {
            try {
                await myFetch(
                    dev + 'memberships/' + this._joinRequest.projectId + '/requests',
                    'PUT',
                    {
                        joinRequestId: this._joinRequest.joinRequestId,
                        status: 'ACCEPTED'
                    }
                )
                alert('Request accepted!')
                window.location.reload()
            } catch (err) {
                console.error(err)
                alert(err.response ? err.response.data.message : 'Something went wrong')
            }
        }

        const rejectRequest = async () => {
            try {
                await myFetch(
                    dev + 'memberships/' + this._joinRequest.projectId + '/requests',
                    'PUT',
                    {
                        joinRequestId: this._joinRequest.joinRequestId,
                        status: 'REJECTED'
                    }
                )
                alert('Request rejected!')
                window.location.reload()
            } catch (err) {
                console.error(err)
                alert(err.response ? err.response.data.message : 'Something went wrong')
            }
        }

        const undoRejection = async () => {
            try {
                await myFetch(
                    dev + 'memberships/' + this._joinRequest.projectId + '/requests',
                    'PUT',
                    {
                        joinRequestId: this._joinRequest.joinRequestId,
                        status: 'PENDING'
                    }
                )
                alert('Request is pending again.')
                window.location.reload()
            } catch (err) {
                console.error(err)
                alert(err.response ? err.response.data.message : 'Something went wrong')
            }
        }

        // Accept a pending join request
        if (this.shadowRoot.querySelector('.btn-accept') != null) {
            const acceptBtn = this.shadowRoot.querySelector('.btn-accept')
            acceptBtn.addEventListener('click', acceptRequest)
        }

        if (this.shadowRoot.querySelector('.btn-reject') != null) {
            const rejectBtn = this.shadowRoot.querySelector('.btn-reject')
            rejectBtn.addEventListener('click', rejectRequest)
        }

        if (this.shadowRoot.querySelector('.btn-undo') != null) {
            const undoBtn = this.shadowRoot.querySelector('.btn-undo')
            undoBtn.addEventListener('click', undoRejection)
        }
    }

    render() {
        this.shadowRoot.innerHTML = `${this.style}${this.template}`
        this.callFunctions()
    }
}

customElements.define('c-join-request-item', JoinRequestItem)
