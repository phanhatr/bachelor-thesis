const template = document.createElement('template')
template.innerHTML = /*html*/ `
<style>
.divider {
    display: block; 
    width: 0.75rem; 
    height: 0.75rem;     
}
</style>

<div class='divider'>
</div>
`

class Divider extends HTMLElement {
  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
    this.shadowRoot.appendChild(template.content.cloneNode(true))
  }
}

customElements.define('c-divider', Divider)
