import { myFetch } from './FetchFunction.js'
import { dev } from './config.js'

class NonMemberView extends HTMLElement {
  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
  }

  _project = []

  get project () {
    return this._project || []
  }

  set project (value) {
    this._project = value
    this.render()
  }

  _myProjectRequestStatus = []

  get myProjectRequestStatus () {
    return this._myProjectRequestStatus || []
  }

  set myProjectRequestStatus (value) {
    this._myProjectRequestStatus = value
    this.render()
  }

  get style () {
    return /*css*/ ` 
        <style>
        @import url('https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap');

        * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
        font-family: "Inter", sans-serif;
      }
      
      body {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        color: #222;
        position: relative;
        min-height: 100vh;
        background-color: #b3e6f4;
      }
      
      .modal {
        display: flex;
        flex-direction: column;
        justify-content: center;
        gap: 0.4rem;
        padding: 1.3rem;
        min-height: 250px;
        position: absolute;
        z-index: 2;
        top: 30%;
        background-color: white;
        border: 1px solid #ddd;
        border-radius: 15px;
        left: 50%;
        margin-left: -250px;
      }
      
      .modal .flex {
        display: flex;
        align-items: center;
        justify-content: space-between;
      }
      
      .modal input {
        padding: 0.7rem 1rem;
        border: 1px solid #ddd;
        border-radius: 5px;
        font-size: 0.9em;
      }
      
      .modal p {
        font-size: 0.9rem;
        color: #777;
        margin: 0.4rem 0 0.2rem;
      }

      .view-buttons {
        text-align: right;
      }
      
      .btn {
        background-color: #121212;
        background-image: none;
        border: 1px solid #000;
        border-radius: 4px;
        box-shadow: #fff 4px 4px 0 0,#000 4px 4px 0 1px;
        box-sizing: border-box;
        color: #fff;
        cursor: pointer;
        display: inline-block;
        font-family: 'Inter', sans-serif;
        font-size: 14px;
        font-weight: bold;
        line-height: 20px;
        margin: 0 5px 10px 0;
        overflow: visible;
        padding: 12px 40px;
        text-align: center;
        text-transform: none;
        touch-action: manipulation;
        user-select: none;
        -webkit-user-select: none;
        vertical-align: middle;
        white-space: nowrap;
      }

      .request-pending {
        padding: 0.6rem;
        margin-left: 0.5rem;
        background-color: #dfd6f2;
        color: #716389;
        font-weight: 700;
        border-radius: 0.375rem;
      }

      .request-rejected {
        padding: 0.6rem;
        margin-left: 0.5rem;
        background-color: #f3d8e6;
        color: #6a0c15;
        font-weight: 700;
        border-radius: 0.375rem;
      }

      .btn-cancel {
        display: inline-block;
        padding: 0.8rem 1.4rem;
        font-weight: bold;
        background-color: #fef6e1;
        color: #6e6e6e;
        border-radius: 5px;
        text-align: center;
        margin-top: 1rem;
      }

      .btn-modal {
        text-align: center;
        margin-top: 1rem;
        width: 280px;
      }

      .btn-close {
        display: inline-block;
        padding: 0.8rem 1.4rem;
        font-weight: bold;
        background-color: #fef6e1;
        color: #6e6e6e;
        border-radius: 5px;
      }

      .btn-send {
        display: inline-block;
        padding: 0.8rem 1.4rem;
        font-weight: bold;
        background-color: #c0a6f7;
        color: #000000;
        border-radius: 5px;
      }

      .btn-open {
        margin-top: 1rem;
      }
      
      .overlay {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        width: 100%;
        height: 100%;
        background: rgba(0, 0, 0, 0.5);
        backdrop-filter: blur(3px);
        z-index: 1;
      }
      
      textarea {
      padding-top: 0.5rem;
      padding-bottom: 0.5rem; 
      padding-left: 1rem;
      padding-right: 1rem; 
      width: 100%; 
      border-radius: 0.375rem; 
      border-width: 1px; 
      border-color: #D1D5DB; 
      box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.05); 
      }
      
      .title {
      margin-bottom: 1.5rem; 
      font-size: 1.25rem;
      line-height: 1.75rem; 
      font-weight: 500; 
      text-align: center; 
      }
      
      .buttons{
        margin: auto;
      }

      .message{
      display: block; 
      margin-bottom: 0.25rem; 
      font-weight: 500; 
      }
      
      .write {
      margin-bottom: 0.75rem; 
      color: #6B7280; 
      font-size: 0.875rem;
      line-height: 1.25rem; 
      font-weight: 400; 
      width: 100%; 
      }
      
      .hidden {
        display: none;
      }
      
      .project-name {
          color: #6f1296;
          font-weight: 500;
      }
        </style>
      `
  }

  callFunctions () {
    const modal = this.shadowRoot.querySelector('.modal')
    const overlay = this.shadowRoot.querySelector('.overlay')
    const closeModalBtn = this.shadowRoot.querySelector('.btn-close')
    const sendBtn = this.shadowRoot.querySelector('.btn-send')
    const messageTextarea = this.shadowRoot.querySelector('#textarea')

    const closeModal = function () {
      modal.classList.add('hidden')
      overlay.classList.add('hidden')
    }

    // Close the modal when the close button and overlay is clicked

    closeModalBtn.addEventListener('click', closeModal)
    overlay.addEventListener('click', closeModal)

    // Close modal when the Esc key is pressed

    this.shadowRoot.addEventListener('keydown', function (e) {
      if (e.key === 'Escape' && !modal.classList.contains('hidden')) {
        closeModal()
      }
    })

    // open modal function
    const openModal = function () {
      modal.classList.remove('hidden')
      overlay.classList.remove('hidden')
    }

    const sendRequest = async () => {
      try {
        await myFetch(
          dev + 'memberships/' + this._project.projectId + '/join',
          'POST',
          { message: messageTextarea.value }
        )
        alert('Request sent!')
        this._requests = 'PENDING'
        closeModal()
        window.location.reload()
      } catch (err) {
        console.error(err)
        alert(err.response ? err.response.data.message : 'Something went wrong')
      }
    }

    sendBtn.addEventListener('click', sendRequest)

    // open modal event

    const cancelRequest = async () => {
      try {
        await myFetch(
          dev + 'memberships/' + this._project.projectId + '/cancel',
          'PUT',
          {}
        )
        alert('Request cancelled!')
        this._requests = 'CANCELLED'
        window.location.reload()
      } catch (err) {
        console.error(err)
        alert(err.response ? err.response.data.message : 'Something went wrong')
      }
    }

    // Cancel a pending join request
    if (this.shadowRoot.querySelector('.btn-cancel') != null) {
      const cancelBtn = this.shadowRoot.querySelector('.btn-cancel')
      cancelBtn.addEventListener('click', cancelRequest)
    }

    // Request to join if no request exists
    if (this.shadowRoot.querySelector('.btn-open') != null) {
      const openModalBtn = this.shadowRoot.querySelector('.btn-open')
      openModalBtn.addEventListener('click', openModal)
    }
  }

  render () {
    this.shadowRoot.innerHTML = /*html*/ `
    ${this.style}
    <section class="modal hidden">
    <div class="title">
      Request to join <span class="project-name">${this._project.name}</span>
    </div>
    <label>
      <span class="message">Message</span>
      <textarea
        rows="4"
        id="textarea"
        placeholder="Write a few sentences about yourself."
        required
        name="message"
      ></textarea>
    </label>

    <p class="write">
      Write a few sentences about yourself.
    </p>
    <div class="buttons">
      <button class="btn btn-close btn-modal" role="button">
        Cancel
      </button>
      <button class="btn btn-send btn-modal" role="button">
      Send request
    </button>
    </div>
  </section>

  <div class="overlay hidden"></div>
  <div class="view-buttons">
  ${
    this._myProjectRequestStatus === 'PENDING'
      ? `<div class="request-pending">Your request is waiting to be resolved.</div>
      <button class="btn btn-cancel">Cancel request</button>`
      : this._myProjectRequestStatus === 'REJECTED'
      ? `<div class="request-rejected">Sorry, your request to join has been rejected.</div>`
      : `<button class="btn btn-open">Request to join</button>`
  }
  </div>
    `
    this.callFunctions()
  }
}

customElements.define('c-non-member-view', NonMemberView)
