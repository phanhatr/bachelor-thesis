import {myFetch} from './FetchFunction.js'
import {dev} from './config.js'

const emailEl = document.querySelector('#email')

const form = document.querySelector('#forgot-form')

const checkEmail = () => {
    let valid = false
    const email = emailEl.value.trim()
    if (!isRequired(email)) {
        showError(emailEl, 'Email cannot be blank.')
    } else if (!isEmailValid(email)) {
        showError(emailEl, 'Email is not valid.')
    } else {
        showSuccess(emailEl)
        valid = true
    }
    return valid
}
const isEmailValid = email => {
    const re =
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
}


const isRequired = value => value !== ''

const showError = (input, message) => {
    // get the form-field element
    const formField = input.parentElement
    // add the error class
    input.classList.remove('success')
    input.classList.add('error')

    // show the error message
    const error = formField.querySelector('small')
    error.textContent = message
}

const showSuccess = input => {
    // get the form-field element
    const formField = input.parentElement

    // remove the error class
    input.classList.remove('error')
    input.classList.add('success')

    // hide the error message
    const error = formField.querySelector('small')
    error.textContent = ''
}

form.addEventListener('submit', function (e) {
    // prevent the form from submitting
    e.preventDefault()

    // validate fields
    let isFormValid = checkEmail()

    // submit to the server if the form is valid
    if (isFormValid) {
        const sendRequest = async () => {
            try {
                await myFetch(dev + 'users/forgot/password', 'POST', {
                    userEmail: emailEl.value.trim(),
                })
                alert('Password reset is successful')
                window.location.href = 'http://localhost:8080/login.html'
            } catch (err) {
                console.error(err)
                alert(err.response ? err.response.data.message : 'Something went wrong')
            }
        }
        sendRequest()
    }
})

const debounce = (fn, delay = 500) => {
    let timeoutId
    return (...args) => {
        // cancel the previous timer
        if (timeoutId) {
            clearTimeout(timeoutId)
        }
        // setup a new timer
        timeoutId = setTimeout(() => {
            fn.apply(null, args)
        }, delay)
    }
}

form.addEventListener(
    'input',
    debounce(function (e) {
        switch (e.target.id) {
            case 'email':
                checkEmail()
                break
        }
    })
)
