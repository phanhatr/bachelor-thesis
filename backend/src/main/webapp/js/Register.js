import { myFetch } from './FetchFunction.js'
import { dev } from './config.js'

const emailEl = document.querySelector('#email')
const firstNameEl = document.querySelector('#first-name')
const lastNameEl = document.querySelector('#last-name')
const passwordEl = document.querySelector('#password')
const confirmPasswordEl = document.querySelector('#confirm-password')
const userDescriptionEl = document.querySelector('#user-description')

const form = document.querySelector('#register-form')

const checkfirstName = () => {
  let valid = false

  const min = 2,
    max = 40

  const firstName = firstNameEl.value.trim()

  if (!isRequired(firstName)) {
    showError(firstNameEl, 'First name cannot be blank.')
  } else if (!isBetween(firstName.length, min, max)) {
    showError(
      firstNameEl,
      `First name must be between ${min} and ${max} characters.`
    )
  } else {
    showSuccess(firstNameEl)
    valid = true
  }
  return valid
}

const checklastName = () => {
  let valid = false

  const min = 2,
    max = 40

  const lastName = lastNameEl.value.trim()

  if (!isRequired(lastName)) {
    showError(lastNameEl, 'First name cannot be blank.')
  } else if (!isBetween(lastName.length, min, max)) {
    showError(
      lastNameEl,
      `Last name must be between ${min} and ${max} characters.`
    )
  } else {
    showSuccess(lastNameEl)
    valid = true
  }
  return valid
}

const checkEmail = () => {
  let valid = false
  const email = emailEl.value.trim()
  if (!isRequired(email)) {
    showError(emailEl, 'Email cannot be blank.')
  } else if (!isEmailValid(email)) {
    showError(emailEl, 'Email is not valid.')
  } else {
    showSuccess(emailEl)
    valid = true
  }
  return valid
}

const checkPassword = () => {
  let valid = false

  const password = passwordEl.value.trim()

  if (!isRequired(password)) {
    showError(passwordEl, 'Password cannot be blank.')
  } else if (!isPasswordSecure(password)) {
    showError(
      passwordEl,
      'Password must has at least 8 characters that include at least 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character in (!@#$%^&*)'
    )
  } else {
    showSuccess(passwordEl)
    valid = true
  }

  return valid
}

const checkConfirmPassword = () => {
  let valid = false
  // check confirm password
  const confirmPassword = confirmPasswordEl.value.trim()
  const password = passwordEl.value.trim()

  if (!isRequired(confirmPassword)) {
    showError(confirmPasswordEl, 'Please enter the password again')
  } else if (password !== confirmPassword) {
    showError(confirmPasswordEl, 'The password does not match')
  } else {
    showSuccess(confirmPasswordEl)
    valid = true
  }

  return valid
}

const isEmailValid = email => {
  const re =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}

const isPasswordSecure = password => {
  const re = new RegExp(
    '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})'
  )
  return re.test(password)
}

const isRequired = value => value !== ''
const isBetween = (length, min, max) => !(length < min || length > max)

const showError = (input, message) => {
  // get the form-field element
  const formField = input.parentElement
  // add the error class
  input.classList.remove('success')
  input.classList.add('error')

  // show the error message
  const error = formField.querySelector('small')
  error.textContent = message
}

const showSuccess = input => {
  // get the form-field element
  const formField = input.parentElement

  // remove the error class
  input.classList.remove('error')
  input.classList.add('success')

  // hide the error message
  const error = formField.querySelector('small')
  error.textContent = ''
}

form.addEventListener('submit', function (e) {
  // prevent the form from submitting
  e.preventDefault()

  // validate fields
  let isfirstNameValid = checkfirstName(),
    islastNameValid = checklastName(),
    isEmailValid = checkEmail(),
    isPasswordValid = checkPassword(),
    isConfirmPasswordValid = checkConfirmPassword()

  let isFormValid =
    isfirstNameValid &&
    islastNameValid &&
    isEmailValid &&
    isPasswordValid &&
    isConfirmPasswordValid

  // submit to the server if the form is valid
  if (isFormValid) {
    const sendRequest = async () => {
      try {
        await myFetch(dev + 'users/register', 'POST', {
          firstName: firstNameEl.value.trim(),
          lastName: lastNameEl.value.trim(),
          email: emailEl.value.trim(),
          passwordHash: confirmPasswordEl.value.trim(),
          description: userDescriptionEl.value.trim()
        })
        alert('User registration successful')
        window.location.href = '/secured/home.html'
      } catch (err) {
        console.error(err)
        alert(err.response ? err.response.data.message : 'Something went wrong')
      }
    }
    sendRequest()
  }
})

const debounce = (fn, delay = 500) => {
  let timeoutId
  return (...args) => {
    // cancel the previous timer
    if (timeoutId) {
      clearTimeout(timeoutId)
    }
    // setup a new timer
    timeoutId = setTimeout(() => {
      fn.apply(null, args)
    }, delay)
  }
}

form.addEventListener(
  'input',
  debounce(function (e) {
    switch (e.target.id) {
      case 'first-name':
        checkfirstName()
        break
      case 'last-name':
        checklastName()
        break
      case 'email':
        checkEmail()
        break
      case 'password':
        checkPassword()
        break
      case 'confirm-password':
        checkConfirmPassword()
        break
    }
  })
)
