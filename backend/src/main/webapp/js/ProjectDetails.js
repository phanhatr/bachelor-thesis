import './Project.js'
import './Divider.js'
import './Logout.js'
import { fetchProjectDetails } from './fetchApi.js'

window.addEventListener('load', () => {
  fetchProjectDetails()
})
