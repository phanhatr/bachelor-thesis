import './Logout.js'
import { selectBoxTransform } from './SelectBox.js'
import { myFetch } from './FetchFunction.js'
import { dev } from './config.js'

const projectTitleEl = document.querySelector('#project-title')
const maxMembersEl = document.querySelector('#max-members')
const projectDescriptionEl = document.querySelector('#project-description')
const positionSelectEL = document.querySelector('.wanted-positions-select')

const categoryRadiosEl = document.getElementById('category-radios')

window.addEventListener('load', () => {
  fetchProjects()
})

let checkedCategory

const isRequired = value => value !== ''

function htmlEncode (str) {
  return String(str).replace(/[^\w. ]/gi, function (c) {
    return '&#' + c.charCodeAt(0) + ';'
  })
}

function jsEscape (str) {
  return String(str).replace(/[^\w. ]/gi, function (c) {
    return '\\u' + ('0000' + c.charCodeAt(0).toString(16)).slice(-4)
  })
}

export async function fetchPositions () {
  const res = await fetch(dev + 'positions')
  const json = await res.json()

  for (let i = 0; i < json.length; i++) {
    let option = document.createElement('option')
    option.value = json[i].positionsWantedId
    option.text = json[i].positionsWantedName
    if (json[i].id === 0) {
      option.selected = true
    }
    positionSelectEL.appendChild(option)
  }
  selectBoxTransform()
}

export async function fetchCategories () {
  const res = await fetch(dev + 'categories')
  const json = await res.json()
  categoryRadiosEl.innerHTML = Array.from(json)
    .map(
      category => `
        <input
          type="radio"
          name="category"
          value=${category.categoryId}
          id=${'category-' + category.categoryName}
        />
        <label class="m-b-xs" for=${'category-' + category.categoryName}>
          ${category.categoryName}
        </label>`
    )
    .join(' ')

  function getSelected () {
    if (this.checked) {
      checkedCategory = {
        categoryId: this.value,
        categoryName: this.labels[0].textContent.trim()
      }
    }
  }

  // add an event listener for the change event
  const categoriesEl = document.querySelectorAll('input[name="category"]')
  for (const categoryEl of categoriesEl) {
    categoryEl.addEventListener('change', getSelected)
  }
}

window.addEventListener('load', () => {
  fetchPositions()
  fetchCategories()
})

const form = document.querySelector('#new-project-form')

form.addEventListener('submit', function () {
  var selectedPositions = []
  for (var option of positionSelectEL.options) {
    if (option.selected) {
      const foo = {
        positionsWantedId: option.value,
        positionsWantedName: option.text,
        positionsWantedDescription: option.text
      }
      selectedPositions.push(foo)
    }
  }

  const sendRequest = async () => {
    try {
      await myFetch(dev + 'projects', 'POST', {
        name: htmlEncode(projectTitleEl.value.trim()),
        maxMembers: maxMembersEl.value.trim(),
        category: checkedCategory,
        description: htmlEncode(projectDescriptionEl.value),
        positionsWanted: selectedPositions
      })
      alert('Your project has been created!')
      window.location.replace('/secured/myprojects.html')
    } catch (err) {
      console.error(err)
      alert(err.response ? err.response.data.message : 'Something went wrong')
    }
  }
  sendRequest()
})

const checkProjectTitle = () => {
  let valid = false

  const min = 2,
    max = 255

  const projectTitle = projectTitleEl.value.trim()

  if (!isRequired(projectTitle)) {
    showError(projectTitleEl, 'Project title cannot be blank.')
  } else if (!isBetween(projectTitle.length, min, max)) {
    showError(
      projectTitleEl,
      `Project title must be between ${min} and ${max} characters.`
    )
  } else {
    showSuccess(projectTitleEl)
    valid = true
  }
  return valid
}

const checkProjectDescription = () => {
  let valid = false

  const min = 2,
    max = 2500

  const projectDescription = projectDescriptionEl.value.trim()

  if (!isRequired(projectDescription)) {
    showError(projectDescriptionEl, 'Project description cannot be blank.')
  } else if (!isBetween(projectDescription.length, min, max)) {
    showError(
      projectDescriptionEl,
      `Project description name must be between ${min} and ${max} characters.`
    )
  } else {
    showSuccess(projectDescriptionEl)
    valid = true
  }
  return valid
}

form.addEventListener('submit', function (e) {
  // prevent the form from submitting
  e.preventDefault()

  // validate fields
  let isProjectTitleValid = checkProjectTitle(),
    isProjectDescriptionValid = checkProjectDescription()

  let isFormValid =
    isProjectTitleValid && isProjectDescriptionValid && isProjectTitleValid

  // submit to the server if the form is valid
  if (isFormValid) {
    const sendRequest = async () => {
      try {
        await myFetch(dev + 'users/register', 'POST', {
          projectTitle: projectTitleEl.value.trim(),
          projectDescription: projectDescriptionEl.value.trim(),
          email: emailEl.value.trim(),
          passwordHash: confirmPasswordEl.value.trim(),
          description: userDescriptionEl.value.trim()
        })
        alert('User registration successful')
        window.location.href = 'http://localhost:8080/login.html'
      } catch (err) {
        console.error(err)
        alert(err.response ? err.response.data.message : 'Something went wrong')
      }
    }
    sendRequest()
  }
})

const debounce = (fn, delay = 500) => {
  let timeoutId
  return (...args) => {
    // cancel the previous timer
    if (timeoutId) {
      clearTimeout(timeoutId)
    }
    // setup a new timer
    timeoutId = setTimeout(() => {
      fn.apply(null, args)
    }, delay)
  }
}

form.addEventListener(
  'input',
  debounce(function (e) {
    switch (e.target.id) {
      case 'project-title':
        checkProjectTitle()
        break
      case 'project-description':
        checkProjectDescription()
        break
    }
  })
)
