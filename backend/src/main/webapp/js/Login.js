const emailEl = document.querySelector('#email')
const passwordEl = document.querySelector('#password')
const loginErrorEl = document.querySelector('#login-error')
const form = document.querySelector('#login-form')

let errorParam = new URLSearchParams(window.location.search).get('error')

if (errorParam) {
  var errorMessage = document.createElement('div')
  loginErrorEl.innerText = 'Invalid username or password'
  loginErrorEl.classList.add('error-message')
}

// Add event listener to form submit event
form.addEventListener('submit', function (event) {
  const email = emailEl.value.trim()
  const password = passwordEl.value.trim()

  var newUrl = '/secured/home.html' // The new URL you want to set
  // Change the URL without adding a new history entry

  // Change the URL without redirecting
  history.pushState(null, null, newUrl)

  // Validate form data
  if (email == '' || password == '') {
    event.preventDefault()
    loginErrorEl.innerHTML = 'Email and password are required'
    loginErrorEl.style.display = 'block'
  } else {
    loginErrorEl.style.display = 'none'
  }
})
