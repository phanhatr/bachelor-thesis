import {myFetch} from './FetchFunction'
import {dev} from './config.js'

const template = document.createElement('template')
template.innerHTML = /*html*/ `
<style>
</style>
<div class='project-requests'>
  <div class='project-request-author'></div>
  <div class='project-request-message'></div>
  <div class="buttons">
    <button class="btn-accept">
      Accept
    </button>
    <button class="btn-reject">
      Reject
    </button>
  </div>
</div>
`

class ProjectRequestItem extends HTMLElement {
    constructor() {
        super()

        this.attachShadow({mode: 'open'})
        this.shadowRoot.appendChild(template.content.cloneNode(true))
        this.shadowRoot.querySelector('.project-request-author').innerText =
            this.getAttribute('author')
        this.shadowRoot.querySelector('.project-request-message').innerText =
            this.getAttribute('message')

        const acceptRequest = async () => {
            try {
                await myFetch(
                    dev + 'memberships/' + this.getAttribute('project') + '/requests',
                    'PUT',
                    {status: 'ACCEPTED'}
                )
                alert('Request accepted')
            } catch (err) {
                console.error(err)
                alert(err.response ? err.response.data.message : 'Something went wrong')
            }
        }

        sendBtn.addEventListener('click', acceptRequest)

        const rejectRequest = async () => {
            try {
                await myFetch(
                    dev + 'memberships/' + this.getAttribute('project') + '/requests',
                    'PUT',
                    {status: 'REJECTED'}
                )
                alert('Request rejected')
            } catch (err) {
                console.error(err)
                alert(err.response ? err.response.data.message : 'Something went wrong')
            }
        }

        // Cancel a pending join request
        if (this.shadowRoot.querySelector('.btn-reject') != null) {
            const rejectBtn = this.shadowRoot.querySelector('.btn-reject')
            rejectBtn.addEventListener('click', rejectRequest)
        }
        if (this.shadowRoot.querySelector('.btn-accept') != null) {
            const acceptBtn = this.shadowRoot.querySelector('.btn-accept')
            acceptBtn.addEventListener('click', acceptRequest)
        }
    }
}

customElements.define('c-project-request-item', ProjectRequestItem)
