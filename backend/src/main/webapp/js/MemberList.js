class MemberList extends HTMLElement {
  _members = []

  constructor () {
    super()

    this.attachShadow({ mode: 'open' })
  }

  get members () {
    return this._members || []
  }

  set members (value) {
    this._members = value
    this.render()
  }

  connectedCallback () {}

  get style () {
    return /*css*/ `
        <style>
        </style>
      `
  }

  get template () {
    const Icon = icons[this._project.category.name]

    return /*html*/ `
      `
  }

  render () {
    this.shadowRoot.innerHTML = `${this.style}${this.template}`
  }
}

customElements.define('c-member-list', MemberList)
