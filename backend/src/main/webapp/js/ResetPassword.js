import {myFetch} from './FetchFunction.js'
import {dev} from './config.js'

const urlParams = new URLSearchParams(window.location.search);
const token = urlParams.get('token')

window.addEventListener('load', () => {
    const checkToken = async () => {
        try {
            await myFetch(dev + 'users/check/token', 'POST', token)
        } catch (e) {
            if (e.status === 401) location.replace("http://localhost:8080/expired.html")
            console.error(e)
            alert(e.response ? e.response.data.message : 'Something went wrong')
        }
    }
    checkToken();
})

const tokenFieldEl = document.querySelector('#tokenId');
tokenFieldEl.value = token;

const newPasswordEl = document.querySelector('#new-password')
const confirmNewPasswordEl = document.querySelector('#confirm-new-password')

const form = document.querySelector('#change-password-form')

const checkPassword = () => {
    let valid = false

    const password = newPasswordEl.value.trim()

    if (!isRequired(password)) {
        showError(newPasswordEl, 'Password cannot be blank.')
    } else if (!isPasswordSecure(password)) {
        showError(
            newPasswordEl,
            'Password must has at least 8 characters that include at least 1 lowercase character, 1 uppercase characters, 1 number, and 1 special character in (!@#$%^&*)'
        )
    } else {
        showSuccess(newPasswordEl)
        valid = true
    }

    return valid
}

const checkConfirmPassword = () => {
    let valid = false
    // check confirm password
    const confirmPassword = confirmNewPasswordEl.value.trim()
    const password = newPasswordEl.value.trim()

    if (!isRequired(confirmPassword)) {
        showError(confirmNewPasswordEl, 'Please enter the password again')
    } else if (password !== confirmPassword) {
        showError(confirmNewPasswordEl, 'The password does not match')
    } else {
        showSuccess(confirmNewPasswordEl)
        valid = true
    }

    return valid
}

const isPasswordSecure = password => {
    const re = new RegExp(
        '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})'
    )
    return re.test(password)
}

const isRequired = value => value !== ''

const showError = (input, message) => {
    // get the form-field element
    const formField = input.parentElement
    // add the error class
    if (input.classList.contains('success')) input.classList.remove('success')
    input.classList.add('error')

    // show the error message
    const error = formField.querySelector('small')
    error.textContent = message
}

const showSuccess = input => {
    // get the form-field element
    const formField = input.parentElement

    // remove the error class
    if (input.classList.contains('error')) input.classList.remove('error')
    input.classList.add('success')

    // hide the error message
    const error = formField.querySelector('small')
    error.textContent = ''
}

form.addEventListener('submit', function (e) {
    // prevent the form from submitting
    e.preventDefault()

    // validate fields
    let isNewPasswordValid = checkPassword(),
        isConfirmNewPasswordValid = checkConfirmPassword()

    let isFormValid = isNewPasswordValid && isConfirmNewPasswordValid

    // submit to the server if the form is valid
    if (isFormValid) {
        const sendRequest = async () => {
            try {
                await myFetch(dev + 'users/reset/password', 'POST', {
                    tokenUUID: tokenFieldEl.value.trim(),
                    newPassword: confirmNewPasswordEl.value.trim()
                })
                alert('Password has been successfully changed')
            } catch (err) {
                console.error(err)
                alert(err.response ? err.response.data.message : 'Something went wrong')
            }
        }
        sendRequest()
    }
})

const debounce = (fn, delay = 500) => {
    let timeoutId
    return (...args) => {
        // cancel the previous timer
        if (timeoutId) {
            clearTimeout(timeoutId)
        }
        // set up a new timer
        timeoutId = setTimeout(() => {
            fn.apply(null, args)
        }, delay)
    }
}

form.addEventListener(
    'input',
    debounce(function (e) {
        switch (e.target.id) {
            case 'new-password':
                checkPassword()
                break
            case 'confirm-new-password':
                checkConfirmPassword()
                break
        }
    })
)