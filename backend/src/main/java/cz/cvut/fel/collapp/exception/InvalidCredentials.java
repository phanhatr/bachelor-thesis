package cz.cvut.fel.collapp.exception;

public class InvalidCredentials extends CustomException {

    public InvalidCredentials(String message) {
        super(message);
    }

}
