package cz.cvut.fel.collapp.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class MembershipId implements Serializable {

    @Column(nullable = false, updatable = false)
    private Integer memberships_projectid;

    @Column( nullable = false, updatable = false)
    private Integer memberships_userid;


    public MembershipId() {}

    public MembershipId(Integer memberships_projectid, Integer memberships_userid) {
        this.memberships_projectid = memberships_projectid;
        this.memberships_userid = memberships_userid;
    }

    public Integer getMemberships_projectid() {
        return memberships_projectid;
    }

    public void setMemberships_projectid(Integer projectId) {
        this.memberships_projectid = projectId;
    }

    public Integer getMemberships_userid() {
        return memberships_userid;
    }

    public void setMemberships_userid(Integer userId) {
        this.memberships_userid = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MembershipId that = (MembershipId) o;
        return memberships_projectid.equals(that.memberships_projectid) &&
                memberships_userid.equals(that.memberships_userid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberships_projectid, memberships_userid);
    }
}
