package cz.cvut.fel.collapp.config;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

@Startup
@Singleton
public class JavaEELiquibaseIntegration {

    private static final Logger log = Logger.getLogger(JavaEELiquibaseIntegration.class.getName());
    private static final String STAGE = "development";
    private static final String CHANGELOG_FILE = "/db/changelog-master.xml";

    @PostConstruct
    public void bootstrap() {
        log.warning("JavaEELiquibaseIntegration start");
        System.setProperty("liquibase. logging", "info");
        ResourceAccessor resourceAccessor = new ClassLoaderResourceAccessor(getClass().getClassLoader());
        Connection connection = null;
        try {
            DataSource ds = (DataSource) (new InitialContext().lookup("jdbc/teamCollab"));
            connection = ds.getConnection();
            JdbcConnection jdbcConnection = new JdbcConnection(connection);
            Database db = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);

            Liquibase liquiBase = new Liquibase(CHANGELOG_FILE, resourceAccessor, db);
            liquiBase.update(STAGE);
            log.warning("JavaEELiquibaseIntegration successfully finished");
        } catch (NamingException | LiquibaseException | SQLException ex) {
            log.log(Level.SEVERE, "Failed to upgrade database", ex);
            throw new RuntimeException("Unable to initialize database via Liquibase! " + ex.getMessage(), ex);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    log.log(Level.SEVERE, "Problem closing db connection: " + ex.getMessage(), ex);
                }
            }
        }
    }

}