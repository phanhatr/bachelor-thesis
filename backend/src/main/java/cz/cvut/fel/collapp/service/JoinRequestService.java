package cz.cvut.fel.collapp.service;

import cz.cvut.fel.collapp.dao.JoinRequestDao;
import cz.cvut.fel.collapp.dao.ProjectDao;
import cz.cvut.fel.collapp.dao.UserDao;
import cz.cvut.fel.collapp.entity.JoinRequest;
import cz.cvut.fel.collapp.entity.JoinRequestStatus;
import cz.cvut.fel.collapp.entity.Projects;
import cz.cvut.fel.collapp.entity.User;
import cz.cvut.fel.collapp.exception.ValidationException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Stateless
public class JoinRequestService {
    @EJB
    private JoinRequestDao joinRequestDao;
    @EJB
    private ProjectDao projectDao;
    @EJB
    private UserDao userDao;
    @EJB
    private MembershipService membershipService;

    public JoinRequestService() {
    }

    public JoinRequest findById(Integer id) {
        Objects.requireNonNull(id);
        return joinRequestDao.find(id);
    }

    public JoinRequestStatus findPendingJoinRequestsByProjectAndUser(Projects projects, User user) {
        for (JoinRequest j : projects.getJoinRequests()) {
            if ((j.getUser().getUserId().equals(user.getUserId())) && (j.getStatus() == JoinRequestStatus.PENDING)) {
                return j.getStatus();
            }
        }
        return null;
    }

    public List<JoinRequest> findJoinRequestByUser(User user) {
        return joinRequestDao.findJoinRequestByUser(user);
    }

    public List<JoinRequest> findJoinRequestByProject(Projects projects) {
        return joinRequestDao.findJoinRequestByProject(projects);
    }

    public void persist(JoinRequest joinRequest) {
        Objects.requireNonNull(joinRequest);
        joinRequestDao.persist(joinRequest);
    }

    public void update(JoinRequest joinRequest) {
        joinRequestDao.update(joinRequest);
    }


    public void remove(JoinRequest joinRequest) {
        Objects.requireNonNull(joinRequest);
        joinRequestDao.remove(joinRequest);
    }

    public void postJoinRequest(JoinRequest joinRequest, Projects projects, User user) {
        Objects.requireNonNull(joinRequest);
        Objects.requireNonNull(projects);
        Objects.requireNonNull(user);

        JoinRequest foundJoinRequest = null;

        for (JoinRequest j : projects.getJoinRequests()) {
            if (j.getUser().getUserId().equals(user.getUserId())) {
                if (j.getStatus() == JoinRequestStatus.PENDING) {
                    throw new ValidationException("Request already sent!");
                }
                if (j.getStatus() == JoinRequestStatus.ACCEPTED) {
                    throw new ValidationException("Request already accepted!");
                }
                if (j.getStatus() == JoinRequestStatus.REJECTED) {
                    throw new ValidationException("Request already rejected!");
                }
                foundJoinRequest = j;
                break;
            }
        }

        // scenario: cancels and then tries again
        if (foundJoinRequest != null) {
            JoinRequest cancelledJoinRequest = findById(foundJoinRequest.getJoinRequestId());
            cancelledJoinRequest.setMessage(joinRequest.getMessage());
            cancelledJoinRequest.setStatus(JoinRequestStatus.PENDING);
        } else {
            joinRequest.setUser(user);
            projects.addJoinRequest(joinRequest);
            joinRequest.setProject(projects);
        }

        if (projects.getNumberOfCurrentMembers() == projects.getMaxMembers()) {
            throw new ValidationException("Project full!");
        }

        if (foundJoinRequest != null) {
            joinRequestDao.update(findById(foundJoinRequest.getJoinRequestId()));
        } else {
            joinRequestDao.persist(joinRequest);
        }
        userDao.update(user);
        projectDao.update(projects);
    }

    public void cancelJoinRequest(Projects projects, User user) {
        Objects.requireNonNull(projects);
        Objects.requireNonNull(user);

        for (JoinRequest j : projects.getJoinRequests()) {
            if (j.getUser().getUserId().equals(user.getUserId())) {
                if (j.getStatus() == JoinRequestStatus.CANCELLED) {
                    throw new ValidationException("Request already cancelled!");
                }
                if (j.getStatus() == JoinRequestStatus.ACCEPTED) {
                    throw new ValidationException("Request already accepted!");
                }
                if (j.getStatus() == JoinRequestStatus.REJECTED) {
                    throw new ValidationException("Request already rejected!");
                }

                j.setStatus(JoinRequestStatus.CANCELLED);

                joinRequestDao.update(j);
                projectDao.update(projects);
                return;
            }
        }
        throw new ValidationException("No request sent!");
    }

    public void resolveJoinRequest(Projects projects, JoinRequest joinRequest) {
        Objects.requireNonNull(projects);
        Objects.requireNonNull(joinRequest);

        for (JoinRequest j : projects.getJoinRequests()) {
            if (j.getJoinRequestId().equals(joinRequest.getJoinRequestId())) {
                if ((j.getStatus() == JoinRequestStatus.ACCEPTED)) {
                    throw new ValidationException("Request already accepted!");
                }

                j.setDateTimeResolved(new Date());
                if (joinRequest.getStatus() == JoinRequestStatus.ACCEPTED) {
                    j.setStatus(JoinRequestStatus.ACCEPTED);
                    joinRequestDao.update(j);
                    membershipService.createMembership(projects, j.getUser(), "MEMBER");
                    projectDao.update(projects);
                    userDao.update(j.getUser());
                } else if (joinRequest.getStatus() == JoinRequestStatus.PENDING) {
                    j.setStatus(JoinRequestStatus.PENDING);
                    joinRequestDao.update(j);
                    projectDao.update(projects);
                } else {
                    j.setStatus(JoinRequestStatus.REJECTED);
                    joinRequestDao.update(j);
                    projectDao.update(projects);
                }
                return;
            }
        }

        throw new ValidationException("Invalid request!");
    }
}
