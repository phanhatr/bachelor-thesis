package cz.cvut.fel.collapp.dao;

import cz.cvut.fel.collapp.entity.Category;
import cz.cvut.fel.collapp.entity.Projects;
import cz.cvut.fel.collapp.entity.User;

import javax.ejb.Stateless;
import java.util.List;
import java.util.Objects;

@Stateless
public class ProjectDao extends BaseDao<Projects> {

    public ProjectDao() {
        super(Projects.class);
    }

    @Override
    public List<Projects> findAll() {
        return em.createQuery("SELECT p FROM Projects p", Projects.class).getResultList();
    }

    public List<Projects> findByUser(User user) {
        Objects.requireNonNull(user);
        return em.createNamedQuery("Projects.findByUser", Projects.class).setParameter("users", user).getResultList();
    }

    public List<Projects> findProjectsByCategory(Category category) {
        Objects.requireNonNull(category);
        return em.createNamedQuery("Projects.findByCategory", Projects.class).setParameter("category", category)
                .getResultList();
    }
}
