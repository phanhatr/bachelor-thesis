package cz.cvut.fel.collapp.dto;

import cz.cvut.fel.collapp.entity.PositionsWanted;

public class PositionsWantedDto {
    private Integer positionsWantedId;
    private String positionsWantedName;

    public PositionsWantedDto() {
    }

    public PositionsWantedDto(PositionsWanted positionsWanted) {
        this.positionsWantedId = positionsWanted.getPositionsWantedId();
        this.positionsWantedName = positionsWanted.getPositionsWantedName();
    }

    public Integer getPositionsWantedId() {
        return positionsWantedId;
    }

    public void setPositionsWantedId(Integer positionsWantedId) {
        this.positionsWantedId = positionsWantedId;
    }

    public String getPositionsWantedName() {
        return positionsWantedName;
    }

    public void setPositionsWantedName(String positionsWantedName) {
        this.positionsWantedName = positionsWantedName;
    }
}
