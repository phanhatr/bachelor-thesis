package cz.cvut.fel.collapp.exception;

public class PersistenceException extends CustomException {

    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistenceException(Throwable cause) {
        super(cause);
    }
}
