package cz.cvut.fel.collapp.exception;

public class NotFoundException extends CustomException {

    public NotFoundException(String message) {
        super(message);
    }

    public static NotFoundException create(String resourceName, Object id) {
        return new NotFoundException(resourceName + " identified by " + id + " not found.");
    }
}