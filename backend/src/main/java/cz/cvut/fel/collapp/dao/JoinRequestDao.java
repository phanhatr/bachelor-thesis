package cz.cvut.fel.collapp.dao;

import cz.cvut.fel.collapp.entity.JoinRequest;
import cz.cvut.fel.collapp.entity.Projects;
import cz.cvut.fel.collapp.entity.User;

import javax.ejb.Stateless;
import java.util.List;
import java.util.Objects;

@Stateless
public class JoinRequestDao extends BaseDao<JoinRequest> {

    public JoinRequestDao() {
        super(JoinRequest.class);
    }

    public List<JoinRequest> findJoinRequestByUser(User user) {
        Objects.requireNonNull(user);
        return em.createNamedQuery("JoinRequest.findByUser", JoinRequest.class).setParameter("users", user)
                .getResultList();
    }

    public List<JoinRequest> findJoinRequestByProject(Projects projects) {
        Objects.requireNonNull(projects);
        return em.createNamedQuery("JoinRequest.findByProject", JoinRequest.class).setParameter("project", projects)
                .getResultList();
    }
}
