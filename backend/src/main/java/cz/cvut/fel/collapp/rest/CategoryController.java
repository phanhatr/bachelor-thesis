package cz.cvut.fel.collapp.rest;

import cz.cvut.fel.collapp.dto.CategoryDto;
import cz.cvut.fel.collapp.service.CategoryService;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Path("/categories")
@Produces(MediaType.APPLICATION_JSON)
public class CategoryController {

    private static final Logger LOG = Logger.getLogger(CategoryController.class.getName());
    @EJB
    private CategoryService categoryService;

    public CategoryController( ) {
    }

    @GET
    public List<CategoryDto> getCategories() {
        return categoryService.findAll().stream().map(CategoryDto::new).collect(Collectors.toList());
    }

}
