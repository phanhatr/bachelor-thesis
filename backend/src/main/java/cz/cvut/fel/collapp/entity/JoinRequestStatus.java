package cz.cvut.fel.collapp.entity;

public enum JoinRequestStatus {

    PENDING("REQUEST_PENDING"), ACCEPTED("REQUEST_ACCEPTED"), REJECTED("REQUEST_REJECTED"),
    CANCELLED("REQUEST_CANCELLED");

    private final String name;

    JoinRequestStatus(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
