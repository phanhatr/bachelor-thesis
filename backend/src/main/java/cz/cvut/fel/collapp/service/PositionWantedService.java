package cz.cvut.fel.collapp.service;

import cz.cvut.fel.collapp.dao.PositionWantedDao;
import cz.cvut.fel.collapp.entity.PositionsWanted;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;
import java.util.Objects;

@Stateless
public class PositionWantedService {
    @EJB
    private PositionWantedDao positionWantedDao;

    public PositionWantedService() {
    }

    public List<PositionsWanted> findAll() {
        return positionWantedDao.findAll();
    }

    public PositionsWanted findById(Integer id) {
        return positionWantedDao.find(id);
    }

    public void persist(PositionsWanted positionsWanted) {
        Objects.requireNonNull(positionsWanted);
        positionWantedDao.persist(positionsWanted);
    }

    public void update(PositionsWanted positionsWanted) {
        positionWantedDao.update(positionsWanted);
    }

    public void remove(PositionsWanted positionsWanted) {
        Objects.requireNonNull(positionsWanted);
        positionWantedDao.remove(positionsWanted);
    }

}
