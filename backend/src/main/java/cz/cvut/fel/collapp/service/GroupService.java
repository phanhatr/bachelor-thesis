package cz.cvut.fel.collapp.service;

import cz.cvut.fel.collapp.dao.GroupDao;
import cz.cvut.fel.collapp.entity.Groups;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;
import java.util.Objects;

@Stateless
public class GroupService {

    @EJB
    private GroupDao groupDao;

    public GroupService() {
    }

    public List<Groups> findAll() {
        return groupDao.findAll();
    }

    public void persist(Groups groups) {
        Objects.requireNonNull(groups);
        groupDao.persist(groups);
    }

    public void deleteCategory(Groups groups) {
        Objects.requireNonNull(groups);
        groupDao.remove(groups);
    }

    public void update(Groups groups) {
        Objects.requireNonNull(groups);
        groupDao.update(groups);
    }

}
