package cz.cvut.fel.collapp.dto;

import java.util.Date;

import cz.cvut.fel.collapp.entity.JoinRequest;

public class JoinRequestDto {

    private Integer joinRequestId;
    private Integer projectId;
    private String message;
    private String status;
    private UserDto user;
    private Date dateTimeCreated;
    private Date dateTimeResolved;

    public JoinRequestDto() {
    }

    public JoinRequestDto(JoinRequest joinRequest) {
        this.joinRequestId = joinRequest.getJoinRequestId();
        this.projectId = joinRequest.getProject().getProjectId();
        this.message = joinRequest.getMessage();
        this.status = joinRequest.getStatus().name();
        this.dateTimeCreated = joinRequest.getDateTimeCreated();
        this.dateTimeResolved = joinRequest.getDateTimeCreated();
        this.user = new UserDto(joinRequest.getUser());
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getJoinRequestId() {
        return joinRequestId;
    }

    public void setJoinRequestId(Integer joinRequestId) {
        this.joinRequestId = joinRequestId;
    }

    public Date getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setDateTimeCreated(Date dateTimeCreated) {
        this.dateTimeCreated = dateTimeCreated;
    }

    public Date getDateTimeResolved() {
        return dateTimeResolved;
    }

    public void setDateTimeResolved(Date dateTimeResolved) {
        this.dateTimeResolved = dateTimeResolved;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
}
