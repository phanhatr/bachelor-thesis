package cz.cvut.fel.collapp.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "projects")
@NamedQueries({
        @NamedQuery(name = "Projects.findByCategory", query = "SELECT p from Projects p WHERE p.category = :category"),
        @NamedQuery(name = "Projects.findByUser", query = "" + "SELECT DISTINCT p from Projects p "
                + "LEFT JOIN Membership m ON m.projects = p " + "WHERE p.projectCreator = :users "
                + "OR (m.membershipState = cz.cvut.fel.collapp.entity.MembershipState.ACTIVE AND m.user = :users)") })
public class Projects {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "projectId")
    private Integer projectId;

    @Column(name = "project_description")
    private String description;

    @Column(name = "project_name", nullable = false)
    private String name;

    @Column(name = "max_members", nullable = false)
    private int maxMembers;


    @ManyToOne(optional = false)
    @JoinColumn(name = "projectcreator_userid", referencedColumnName = "userid")
    private User projectCreator;

    @Column(nullable = false, updatable = false)
    private Date dateTimeCreated;

    @ManyToOne(optional = false)
    private Category category;

    @OneToMany(mappedBy = "projects", cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
    @OrderBy("projects ASC")
    private List<Membership> memberships = new ArrayList<>();

    @OneToMany(mappedBy = "projects", cascade = CascadeType.REMOVE)
    @OrderBy("dateTimeCreated DESC")
    private List<JoinRequest> joinRequests = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "projects_positionswanted",
            joinColumns = @JoinColumn(name = "projects_projectid"),
            inverseJoinColumns = @JoinColumn(name = "positionswanted_positionswantedid"))
    private List<PositionsWanted> positionsWanted = new ArrayList<>();

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxMembers() {
        return maxMembers;
    }

    public void setMaxMembers(int maxMembers) {
        this.maxMembers = maxMembers;
    }

    public List<Membership> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<Membership> membership) {
        this.memberships = membership;
    }

    public void addMembership(Membership membership) {
        this.memberships.add(membership);
    }

    public User getProjectCreator() {
        return projectCreator;
    }

    public void setProjectCreator(User user) {
        this.projectCreator = user;
    }

    public Date getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setDateTimeCreated(Date dateTimeCreated) {
        this.dateTimeCreated = dateTimeCreated;
    }

    public List<JoinRequest> getJoinRequests() {
        return joinRequests;
    }

    public void setJoinRequests(List<JoinRequest> joinRequests) {
        this.joinRequests = joinRequests;
    }

    public void addJoinRequest(JoinRequest joinRequest) {
        this.joinRequests.add(joinRequest);
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getNumberOfCurrentMembers() {
        int activeMemCounter = 0;
        for (Membership membership : memberships) {
            if (membership.getStatus() == MembershipState.ACTIVE) {
                activeMemCounter++;
            }
        }
        return activeMemCounter;
    }

    public List<PositionsWanted> getPositionsWanted() {
        return positionsWanted;
    }

    public void setPositionsWanted(List<PositionsWanted> positionsWanted) {
        this.positionsWanted = positionsWanted;
    }
}
