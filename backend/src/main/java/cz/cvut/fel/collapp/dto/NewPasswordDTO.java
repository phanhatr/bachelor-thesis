package cz.cvut.fel.collapp.dto;

public class NewPasswordDTO {

    private String tokenUUID;
    private String newPassword;

    public NewPasswordDTO(String tokenUUID, String newPassword) {
        this.tokenUUID = tokenUUID;
        this.newPassword = newPassword;
    }

    public NewPasswordDTO() {
    }

    public String getTokenUUID() {
        return tokenUUID;
    }

    public void setTokenUUID(String tokenUUID) {
        this.tokenUUID = tokenUUID;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
