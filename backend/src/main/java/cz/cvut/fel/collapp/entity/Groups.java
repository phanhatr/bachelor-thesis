package cz.cvut.fel.collapp.entity;

import javax.persistence.*;

@Entity
@Table(name = "groups")
public class Groups {

    @Id
    @JoinColumn(name="useremail", referencedColumnName="email")
    private String userEmail;

    @Basic(optional = false)
    @Column(name="groupname", nullable = false)
    private String groupName;

    public Groups(User user, String groupName) {
        this.userEmail = user.getEmail();
        this.groupName = groupName;
    }

    public Groups() {
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
