package cz.cvut.fel.collapp.rest;

import cz.cvut.fel.collapp.dto.PositionsWantedDto;
import cz.cvut.fel.collapp.service.PositionWantedService;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Path("/positions")
@Produces(MediaType.APPLICATION_JSON)
public class PositionWantedController {

    private static final Logger LOG = Logger.getLogger(CategoryController.class.getName());
    @EJB
    private PositionWantedService positionWantedService;

    public PositionWantedController() {
    }

    @GET
    public List<PositionsWantedDto> getPositionWanted() {
        return positionWantedService.findAll().stream().map(PositionsWantedDto::new)
                .collect(Collectors.toList());
    }
}
