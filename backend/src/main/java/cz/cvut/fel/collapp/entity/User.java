package cz.cvut.fel.collapp.entity;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Entity
@Table(name = "users")
@DiscriminatorColumn(discriminatorType = DiscriminatorType.INTEGER, columnDefinition = "TINYINT(1)")
@NamedQueries({@NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email")})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;

    @Basic(optional = false)
    @Column(nullable = false)
    private String firstName;

    @Basic(optional = false)
    @Column(nullable = false)
    private String lastName;

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    private String email;

    @Basic(optional = false)
    @Column(name = "password_hash", nullable = false)
    private String passwordHash;

    @Column
    private String description;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Membership> memberships = new ArrayList<>();

    @OneToMany(mappedBy = "projectCreator", cascade = CascadeType.REMOVE)
    private List<Projects> createdProjects = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE)
    private List<JoinRequest> joinRequests = new ArrayList<>();

    public User() {
    }

    public void addCreatedProject(Projects projects) {
        createdProjects.add(projects);
    }

    @Override
    public String toString() {
        return "User{" + "userId= " + userId + ", firstName='" + firstName + '\'' + ", lastName='" + lastName + '\''
                + ", description='" + description + '\'' + ", email='" + email + '\'' + ", password='" + passwordHash + '\''
                + ", hasMembership='" + memberships + '\'' + ", createdProjects='" + createdProjects + '\'' + '}';
    }

    public String encodeSHA256(String password)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes(StandardCharsets.UTF_8));
        byte[] digest = md.digest();
        return Base64.getEncoder().encodeToString(digest);
    }

    public List<Projects> getCreatedProjects() {
        return createdProjects;
    }

    public void setCreatedProjects(List<Projects> createdProjects) {
        this.createdProjects = createdProjects;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        this.passwordHash = encodeSHA256(password);
    }

    public List<Membership> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<Membership> membership) {
        this.memberships = membership;
    }

    public void addMembership(Membership membership) {
        this.memberships.add(membership);
    }

    public List<JoinRequest> getJoinRequests() {
        return joinRequests;
    }

    public void setJoinRequests(List<JoinRequest> joinRequests) {
        this.joinRequests = joinRequests;
    }
}
