package cz.cvut.fel.collapp.exception;

public class FullProjectException extends CustomException {

    public FullProjectException(String message) {
        super(message);
    }

}
