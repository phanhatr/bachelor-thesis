package cz.cvut.fel.collapp.dao;

import cz.cvut.fel.collapp.entity.Groups;

import javax.ejb.Stateless;

@Stateless
public class GroupDao extends BaseDao<Groups> {

    public GroupDao() {
        super(Groups.class);
    }
}
