package cz.cvut.fel.collapp.service;

import cz.cvut.fel.collapp.dao.PasswordResetDao;
import cz.cvut.fel.collapp.dao.UserDao;
import cz.cvut.fel.collapp.entity.PasswordReset;
import cz.cvut.fel.collapp.entity.User;
import cz.cvut.fel.collapp.exception.InvalidUserException;
import cz.cvut.fel.collapp.exception.NotFoundException;
import org.apache.commons.lang3.RandomStringUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static cz.cvut.fel.collapp.util.EmailUtility.sendEmail;

@Stateless
public class UserService {
    @EJB
    private UserDao userDao;
    @EJB
    private PasswordResetDao passwordResetDao;

    public UserService() {
    }

    public UserService(HttpServletRequest request, HttpServletResponse response) {
    }


    public String resetCustomerPassword(String email) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        User user = userDao.findByEmail(email);

        String randomPassword = RandomStringUtils.randomAlphanumeric(10);

        user.setPasswordHash(randomPassword);
        userDao.update(user);
        return randomPassword;
    }

    public void resetUserPassword(User user) throws InvalidUserException {
        final String uuid = UUID.randomUUID().toString().replace("-", "");

        Date currentDate = new Date();

        // convert date to calendar
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);

        // manipulate date
        c.add(Calendar.DAY_OF_YEAR, 2); //same with c.add(Calendar.DAY_OF_MONTH, 1);

        // convert calendar to date
        Date currentDatePlusTwo = c.getTime();


        PasswordReset passwordReset = new PasswordReset(uuid, user.getEmail(), currentDatePlusTwo);
        sendEmail(user.getEmail(), uuid);
        passwordResetDao.persist(passwordReset);
    }


    public void changeUserPassword(User user, String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        Objects.requireNonNull(user);
        user.setPasswordHash(password);
        userDao.update(user);
    }

    public boolean checkIfValidOldPassword(User user, String currentPassword) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String currentPasswordHash = encodeSHA256(currentPassword);
        return Objects.equals(currentPasswordHash, user.getPasswordHash());
    }

    public boolean checkTokenValidity(String token) throws NotFoundException {
        PasswordReset foundToken = passwordResetDao.findToken(token);
        if (foundToken == null) {
            return false;
        }
        Date currentDate = new Date();
        return currentDate.before(foundToken.getExpirationDate());
    }

    public void resetWithToken(String token, String newPassword) throws NotFoundException, UnsupportedEncodingException, NoSuchAlgorithmException {
        PasswordReset foundToken = passwordResetDao.findToken(token);
        if (foundToken == null) {
            throw NotFoundException.create("Token", token);
        }
        User user = userDao.findByEmail(foundToken.getUserEmail());
        Objects.requireNonNull(user);


        user.setPasswordHash(newPassword);
        userDao.update(user);
        passwordResetDao.remove(foundToken);
    }

    public String encodeSHA256(String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes(StandardCharsets.UTF_8));
        byte[] digest = md.digest();
        return Base64.getEncoder().encodeToString(digest);
    }

    public List<User> findAll() {
        return userDao.findAll();
    }

    public User find(Integer id) {
        return userDao.find(id);
    }

    public void persist(User user) {
        Objects.requireNonNull(user);
        userDao.persist(user);
    }

    public void update(User user) {
        Objects.requireNonNull(user);
        userDao.update(user);
    }

    public void updateDescription(User user, String newDescription) {
        Objects.requireNonNull(user);
        user.setDescription(newDescription);
        userDao.update(user);
    }

    public void remove(User toRemove) {
        Objects.requireNonNull(toRemove);
        userDao.remove(toRemove);
    }

    public User findByEmail(String email) {
        Objects.requireNonNull(email);
        return userDao.findByEmail(email);
    }


}
