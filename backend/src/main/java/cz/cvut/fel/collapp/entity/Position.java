package cz.cvut.fel.collapp.entity;

public enum Position {

    CREATOR("creator"), REGULAR_MEMBER("regular_member");

    private final String name;

    Position(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
