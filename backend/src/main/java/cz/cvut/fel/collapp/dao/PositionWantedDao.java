package cz.cvut.fel.collapp.dao;

import cz.cvut.fel.collapp.entity.PositionsWanted;

import javax.ejb.Stateless;

@Stateless
public class PositionWantedDao extends BaseDao<PositionsWanted> {

    public PositionWantedDao() {
        super(PositionsWanted.class);
    }
}
