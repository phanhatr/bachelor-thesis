package cz.cvut.fel.collapp.dao;

import cz.cvut.fel.collapp.entity.User;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;

@Stateless
public class UserDao extends BaseDao<User> {

    public UserDao() {
        super(User.class);
    }

    public User findByEmail(String email) {
        try {
            return em.createNamedQuery("User.findByEmail", User.class).setParameter("email", email).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}
