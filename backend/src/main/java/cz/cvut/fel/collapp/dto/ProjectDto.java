package cz.cvut.fel.collapp.dto;

import cz.cvut.fel.collapp.entity.JoinRequestStatus;
import cz.cvut.fel.collapp.entity.MembershipState;
import cz.cvut.fel.collapp.entity.Projects;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectDto {
    private Integer projectId;
    private String projectCreator;
    private String name;
    private String description;
    private String dateTimeCreated;
    private CategoryDto category;
    private List<PositionsWantedDto> positionsWanted;
    private int maxMembers;
    private int memberCount;
    private boolean isMember;
    private boolean isCreator;
    private List<MembershipDto> memberships = new ArrayList<>();
    private List<JoinRequestDto> joinRequests = new ArrayList<>();

    public ProjectDto(Projects projects, boolean isMember, boolean isCreator) {
        this.projectId = projects.getProjectId();
        this.projectCreator = projects.getProjectCreator().getFirstName() + " "
                + projects.getProjectCreator().getLastName();
        this.name = projects.getName();
        this.description = projects.getDescription();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        this.dateTimeCreated = formatter.format(projects.getDateTimeCreated());
        this.category = new CategoryDto(projects.getCategory());
        this.positionsWanted = projects.getPositionsWanted().stream().map(PositionsWantedDto::new)
                .collect(Collectors.toList());
        this.maxMembers = projects.getMaxMembers();
        this.memberCount = projects.getNumberOfCurrentMembers();
        this.isMember = isMember;
        this.isCreator = isCreator;

        if (isCreator) {
            this.memberships = projects.getMemberships().stream().filter(m -> m.getStatus() == MembershipState.ACTIVE)
                    .map(MembershipDto::new).collect(Collectors.toList());
            this.joinRequests = projects.getJoinRequests().stream()
                    .filter(j -> j.getStatus() == JoinRequestStatus.PENDING || j.getStatus() == JoinRequestStatus.REJECTED).map(JoinRequestDto::new)
                    .collect(Collectors.toList());
        } else if (isMember) {
            this.memberships = projects.getMemberships().stream().filter(m -> m.getStatus() == MembershipState.ACTIVE)
                    .map(MembershipDto::new).collect(Collectors.toList());
        }
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getProjectCreator() {
        return projectCreator;
    }

    public void setProjectCreator(String projectCreator) {
        this.projectCreator = projectCreator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setDateTimeCreated(String dateTimeCreated) {
        this.dateTimeCreated = dateTimeCreated;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public List<PositionsWantedDto> getPositionsWanted() {
        return positionsWanted;
    }

    public void setPositionsWanted(List<PositionsWantedDto> positionsWanted) {
        this.positionsWanted = positionsWanted;
    }

    public int getMaxMembers() {
        return maxMembers;
    }

    public void setMaxMembers(int maxMembers) {
        this.maxMembers = maxMembers;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }

    public boolean isCreator() {
        return isCreator;
    }

    public void setCreator(boolean creator) {
        isCreator = creator;
    }

    public List<MembershipDto> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<MembershipDto> memberships) {
        this.memberships = memberships;
    }

    public List<JoinRequestDto> getJoinRequests() {
        return joinRequests;
    }

    public void setJoinRequests(List<JoinRequestDto> joinRequests) {
        this.joinRequests = joinRequests;
    }
}
