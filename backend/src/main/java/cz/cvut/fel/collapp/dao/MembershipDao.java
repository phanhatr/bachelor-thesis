package cz.cvut.fel.collapp.dao;

import cz.cvut.fel.collapp.entity.Membership;
import cz.cvut.fel.collapp.entity.MembershipId;

import javax.ejb.Stateless;
import java.util.Objects;

@Stateless
public class MembershipDao extends BaseDao<Membership> {

    public MembershipDao() {
        super(Membership.class);
    }

    public Membership find(MembershipId id) {
        Objects.requireNonNull(id);
        return em.find(type, id);
    }
}
