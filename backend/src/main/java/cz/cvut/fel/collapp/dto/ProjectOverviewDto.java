package cz.cvut.fel.collapp.dto;

import cz.cvut.fel.collapp.entity.Projects;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectOverviewDto {

    private Integer projectId;
    private String projectCreator;
    private String name;
    private String description;
    private Date dateTimeCreated;
    private CategoryDto category;
    private List<PositionsWantedDto> positionsWanted;
    private int maxMembers;
    private int memberCount;

    public ProjectOverviewDto(Projects projects) {
        this.projectId = projects.getProjectId();
        this.projectCreator = projects.getProjectCreator().getFirstName() + " "
                + projects.getProjectCreator().getLastName();
        this.name = projects.getName();
        this.description = projects.getDescription();
        this.dateTimeCreated = projects.getDateTimeCreated();
        this.category = new CategoryDto(projects.getCategory());
        this.positionsWanted = projects.getPositionsWanted().stream().map(PositionsWantedDto::new)
                .collect(Collectors.toList());
        this.maxMembers = projects.getMaxMembers();
        this.memberCount = projects.getNumberOfCurrentMembers();
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getProjectCreator() {
        return projectCreator;
    }

    public void setProjectCreator(String projectCreator) {
        this.projectCreator = projectCreator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setDateTimeCreated(Date dateTimeCreated) {
        this.dateTimeCreated = dateTimeCreated;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public List<PositionsWantedDto> getPositionsWanted() {
        return positionsWanted;
    }

    public void setPositionsWanted(List<PositionsWantedDto> positionsWanted) {
        this.positionsWanted = positionsWanted;
    }

    public int getMaxMembers() {
        return maxMembers;
    }

    public void setMaxMembers(int maxMembers) {
        this.maxMembers = maxMembers;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }
}
