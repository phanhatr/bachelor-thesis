package cz.cvut.fel.collapp.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EmailUtility {
    public static void sendEmail(String to, String UUID) {

        // choose which port
        String dev = "http://localhost:8080/";
        String dep = "http://home.aubrecht.net:9480/";

        final String username = "team.collab.beta@gmail.com";
        final String password = "iwpskzhgcdrhoiyj";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(to)
            );
            message.setSubject("Password reset request");
            message.setText(
                    "Please click on the link to reset your password: \n\n"
                            + dev + "reset-password.html?token=" + UUID
                            + "\n\n If you did not initiate this password reset and have received this email," +
                            " please contact us at team.collab.beta@gmail.com so we can resolve this issue.");

            Transport.send(message);

            System.out.println("Email sent");

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}