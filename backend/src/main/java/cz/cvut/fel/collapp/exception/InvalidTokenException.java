package cz.cvut.fel.collapp.exception;

public class InvalidTokenException extends CustomException {

    public InvalidTokenException(String message) {
        super(message);
    }
}

