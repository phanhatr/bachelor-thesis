package cz.cvut.fel.collapp.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

// todo: finish position wanted

@Entity
@Table(name = "PositionsWanted")
public class PositionsWanted {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer positionsWantedId;

    @Column(name = "positionswanted_name")
    private String positionsWantedName;

    @Column(name = "positionswanted_description")
    private String positionsWantedDescription;

    @ManyToMany(mappedBy = "positionsWanted")
    private List<Projects> projects = new ArrayList<>();

    public void setPositionsWantedId(Integer positionsWantedId) {
        this.positionsWantedId = positionsWantedId;
    }

    public Integer getPositionsWantedId() {
        return positionsWantedId;
    }

    public String getPositionsWantedName() {
        return positionsWantedName;
    }

    public void setPositionsWantedName(String positionsWantedName) {
        this.positionsWantedName = positionsWantedName;
    }

    public String getPositionsWantedDescription() {
        return positionsWantedDescription;
    }

    public void setPositionsWantedDescription(String positionsWantedDescription) {
        this.positionsWantedDescription = positionsWantedDescription;
    }

    public List<Projects> getProjects() {
        return projects;
    }

    public void setProjects(List<Projects> projects) {
        this.projects = projects;
    }
}
