package cz.cvut.fel.collapp.entity;

public enum MembershipState {

    EXPIRED("MEMBERSHIP_EXPIRED"),
    ACTIVE("MEMBERSHIP_ACTIVE");

    private final String name;

    MembershipState(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
