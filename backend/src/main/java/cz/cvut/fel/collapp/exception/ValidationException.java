package cz.cvut.fel.collapp.exception;

public class ValidationException extends CustomException {

    public ValidationException(String message) {
        super(message);
    }
}
