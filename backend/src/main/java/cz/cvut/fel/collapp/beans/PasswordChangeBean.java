package cz.cvut.fel.collapp.beans;

import java.io.Serializable;

public class PasswordChangeBean implements Serializable {
    private String currentPassword;
    private String newPassword;

    public PasswordChangeBean() {
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
