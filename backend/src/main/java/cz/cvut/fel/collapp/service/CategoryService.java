package cz.cvut.fel.collapp.service;

import cz.cvut.fel.collapp.dao.CategoryDao;
import cz.cvut.fel.collapp.entity.Category;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;
import java.util.Objects;

@Stateless
public class CategoryService {

    @EJB
    private CategoryDao categoryDao;

    public CategoryService() {
    }

    public List<Category> findAll() {
        return categoryDao.findAll();
    }

    public Category find(Integer id) {
        return categoryDao.find(id);
    }

    public void persist(Category category) {
        Objects.requireNonNull(category);
        categoryDao.persist(category);
    }

    public void deleteCategory(Category category) {
        Objects.requireNonNull(category);
        categoryDao.remove(category);
    }

    public void update(Category category) {
        Objects.requireNonNull(category);
        categoryDao.update(category);
    }

}
