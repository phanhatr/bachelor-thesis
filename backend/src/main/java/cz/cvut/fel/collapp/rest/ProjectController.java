package cz.cvut.fel.collapp.rest;

import cz.cvut.fel.collapp.dto.ProjectDto;
import cz.cvut.fel.collapp.dto.ProjectOverviewDto;
import cz.cvut.fel.collapp.entity.Membership;
import cz.cvut.fel.collapp.entity.MembershipState;
import cz.cvut.fel.collapp.entity.Projects;
import cz.cvut.fel.collapp.entity.User;
import cz.cvut.fel.collapp.exception.ValidationException;
import cz.cvut.fel.collapp.service.ProjectService;
import cz.cvut.fel.collapp.service.UserService;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.security.enterprise.SecurityContext;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Path("/projects")
@Produces(MediaType.APPLICATION_JSON)
public class ProjectController {

    private static final Logger LOG = Logger.getLogger(ProjectController.class.getName());

    @Inject
    SecurityContext securityContext;

    @EJB
    private ProjectService projectService;

    @EJB
    private UserService userService;

    public ProjectController() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/all")
    public List<ProjectOverviewDto> getProjects() {
        return projectService.findAllProjects().stream().map(ProjectOverviewDto::new)
                .collect(Collectors.toList());
    }

    @GET
    @Path("/myProjects")
    public List<ProjectOverviewDto> getProjectsByUser() {
        String currentUser = securityContext.getCallerPrincipal() != null ? securityContext.getCallerPrincipal().getName() : null;
        User user = userService.findByEmail(currentUser);
        return projectService.findByUser(user).stream().map(ProjectOverviewDto::new)
                .collect(Collectors.toList());
    }

    @GET
    @Path("/{projectId}")
    public ProjectDto getProjectById(@PathParam("projectId") Integer projectId) {
        boolean isMember = false;
        boolean isCreator = false;

        String currentUser = securityContext.getCallerPrincipal() != null ? securityContext.getCallerPrincipal().getName() : null;
        User user = userService.findByEmail(currentUser);
        Projects projects = projectService.findById(projectId);


        for (Membership m : user.getMemberships()) {
            if (m.getProject().getProjectId().equals(projects.getProjectId()) && (m.getStatus() == MembershipState.ACTIVE)) {
                if (m.getProjectPosition().equals("CREATOR")) {
                    isCreator = true;
                    break;
                } else if (m.getProjectPosition().equals("MEMBER")) {
                    isMember = true;
                    break;
                }
            }
        }

        return new ProjectDto(projects, isMember, isCreator);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createProject(Projects projects) {
        try {
            String currentUser = securityContext.getCallerPrincipal() != null ? securityContext.getCallerPrincipal().getName() : null;
            User user = userService.findByEmail(currentUser);
            projectService.createProject(projects, user);
            return Response
                    .ok("Project has been created")
                    .build();
        } catch (ValidationException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return Response.status(500, "Project creation failed due to: " + ex.getMessage()).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{projectId}")
    public void updateProject(Projects projects) {
        projectService.updateProject(projects);
    }

    @DELETE
    @Path("/{projectId}")
    public void deleteProject(@PathParam("projectId") Integer projectId) {
        Projects projects = projectService.findById(projectId);
        projectService.removeProject(projects);
    }
}
