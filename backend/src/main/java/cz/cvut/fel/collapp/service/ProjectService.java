package cz.cvut.fel.collapp.service;

import cz.cvut.fel.collapp.dao.ProjectDao;
import cz.cvut.fel.collapp.dao.UserDao;
import cz.cvut.fel.collapp.entity.*;
import cz.cvut.fel.collapp.exception.ValidationException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Stateless
public class ProjectService {
    @EJB
    private ProjectDao projectDao;
    @EJB
    private UserDao userDao;

    @EJB
    private MembershipService membershipService;

    public ProjectService() {
    }

    public List<Projects> findAllProjects() {
        return projectDao.findAll();
    }

    public List<Projects> findByUser(User user) {
        return projectDao.findByUser(user);
    }

    public List<Projects> findProjectsByCategory(Category category) {
        return projectDao.findProjectsByCategory(category);
    }

    public void persist(Projects projects) {
        projectDao.persist(projects);
    }

    public Projects findById(Integer id) {
        return projectDao.find(id);
    }

    public void update(Projects projects) {
        projectDao.update(projects);
    }

    public void remove(Projects projects) {
        Objects.requireNonNull(projects);
        projectDao.remove(projects);
    }

    public void createProject(Projects projects, User user) {
        projects.setDateTimeCreated(new Date());
        projects.setProjectCreator(user);

        if (projects.getMaxMembers() < 1) {
            throw new ValidationException("Can't have less max members than current members");
        }
        if (projects.getDescription().length() > 666) {
            throw new ValidationException("Only descriptions of max 666 character are supported in Collapp beta");
        }


        projectDao.persist(projects);
        membershipService.createMembership(projects, user, "CREATOR");
        userDao.update(user);
    }

    public void updateProject(Projects projects) {
        Projects ogProjects = findById(projects.getProjectId());
        if (!ogProjects.getProjectId().equals(projects.getProjectId())) {
            throw new ValidationException("Wrong project");
        }
        if (getNumberOfCurrentMembers(ogProjects) > projects.getMaxMembers()) {
            throw new ValidationException("Can't have less max members than current members");
        }
        if (projects.getDescription().length() > 666) {
            throw new ValidationException("Only descriptions of max 300 character are supported in Collapp beta");
        }
        projects.setMemberships(ogProjects.getMemberships());
        projects.setJoinRequests(ogProjects.getJoinRequests());
        projects.setDateTimeCreated(ogProjects.getDateTimeCreated());
        projects.setProjectCreator(ogProjects.getProjectCreator());
        projectDao.update(projects);
    }

    public void removeProject(Projects projects) {
        Objects.requireNonNull(projects);
        projects.setPositionsWanted(new ArrayList<>());
        projectDao.remove(projects);
    }

    private int getNumberOfCurrentMembers(Projects projects) {
        int activeMemeCounter = 0;
        for (Membership m : projects.getMemberships()) {
            if (m.getStatus() == MembershipState.ACTIVE) {
                activeMemeCounter++;
            }
        }
        return activeMemeCounter;
    }


}
