package cz.cvut.fel.collapp.service;

import cz.cvut.fel.collapp.rest.CategoryController;

import javax.ejb.Stateless;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

@Stateless
public class AuthService {

    private static final Logger LOG = Logger.getLogger(CategoryController.class.getName());

    public static String encodeSHA256(String password)
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        String salt = "salted123fish"; //TODO for now
        md.update(password.getBytes(StandardCharsets.UTF_8));
//        md.update(salt.getBytes(StandardCharsets.UTF_8));
        byte[] digest = md.digest();
        return Base64.getEncoder().encodeToString(digest);
    }

    public static void main(String[] args) {
        try {
            System.out.println("SHA256 for 'user'/'scIYclz' = '" + encodeSHA256("Hello12!") + "'");
            System.out.println("SHA256 for 'admin'/'cYxkby9' = '" + encodeSHA256("Adm!n12!") + "'");
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            Logger.getLogger(AuthService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

