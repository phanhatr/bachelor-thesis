package cz.cvut.fel.collapp.exception;

public class InvalidOldPasswordException extends CustomException {
    public InvalidOldPasswordException(String message) {
        super(message);
    }
}
