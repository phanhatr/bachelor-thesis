package cz.cvut.fel.collapp.dao;

import cz.cvut.fel.collapp.entity.Category;
import javax.ejb.Stateless;

@Stateless
public class CategoryDao extends BaseDao<Category> {

    public CategoryDao() {
        super(Category.class);
    }
}
