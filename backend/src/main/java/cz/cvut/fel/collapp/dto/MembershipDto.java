package cz.cvut.fel.collapp.dto;

import cz.cvut.fel.collapp.entity.Membership;

public class MembershipDto {

    private Integer userId;
    private String firstName;
    private String lastName;
    private String email;
    private String projectPosition;

    public MembershipDto() {
    }

    public MembershipDto(Membership membership) {
        this.userId = membership.getUser().getUserId();
        this.firstName = membership.getUser().getFirstName();
        this.lastName = membership.getUser().getLastName();
        this.email = membership.getUser().getEmail();
        this.projectPosition = membership.getProjectPosition();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return projectPosition;
    }

    public void setPosition(String position) {
        this.projectPosition = position;
    }
}
