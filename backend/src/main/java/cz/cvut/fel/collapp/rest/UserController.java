package cz.cvut.fel.collapp.rest;

import cz.cvut.fel.collapp.beans.PasswordChangeBean;
import cz.cvut.fel.collapp.dto.NewPasswordDTO;
import cz.cvut.fel.collapp.dto.UserDto;
import cz.cvut.fel.collapp.dto.UserPrivateDto;
import cz.cvut.fel.collapp.entity.Groups;
import cz.cvut.fel.collapp.entity.PasswordReset;
import cz.cvut.fel.collapp.entity.User;
import cz.cvut.fel.collapp.exception.InvalidOldPasswordException;
import cz.cvut.fel.collapp.exception.InvalidTokenException;
import cz.cvut.fel.collapp.exception.InvalidUserException;
import cz.cvut.fel.collapp.service.GroupService;
import cz.cvut.fel.collapp.service.UserService;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.security.enterprise.SecurityContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("/users")
public class UserController {

    private static final Logger LOG = Logger.getLogger(UserController.class.getName());
    @Inject
    SecurityContext securityContext;
    @EJB
    private UserService userService;
    @EJB
    private GroupService groupService;
    @Inject
    private HttpServletRequest request;

    public UserController() {
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("register")
    public void registerUser(User user) {
        userService.persist(user);
        Groups groups = new Groups(user, "user");
        groupService.persist(groups);
        Logger.getLogger(UserController.class.getName()).log(Level.INFO, () -> user + "successfully registered.");
    }

    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response login(@QueryParam("username") String username, @QueryParam("password") String password) {
        try {
            request.getSession(true);
            // login
            request.login(username, password);
            return Response.ok("Login OK").build();
        } catch (ServletException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, ex, () -> "Login failed for user '" + username + "' with reason: " + ex.getMessage());
            return Response.status(401 /*Unauthorized*/, "Login failed with reason: " + ex.getMessage()).build();
        }
    }

    @POST
    @Path("logout")
    @Produces(MediaType.TEXT_PLAIN)
    public Response logout() {
        try {
            request.logout();
            HttpSession session = request.getSession(false);
            if (session != null) {
                session.invalidate();
            }
            // A good idea is to remove also the cookie from client
            NewCookie removeJSessionIDCookie = new NewCookie("JSESSIONID", null, request.getContextPath(), "", NewCookie.DEFAULT_VERSION, null, 0, new Date(), false, true);
            return Response.ok("User logged out.").cookie(removeJSessionIDCookie).build();
        } catch (ServletException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return Response.status(500, "Logout failed with reason: " + ex.getMessage()).build();
        }
    }

    @GET
    @Path("me")
    public Response me() {
        UserDto meInfo;
        if (securityContext.getCallerPrincipal() != null) {
            meInfo = new UserDto(securityContext.getCallerPrincipal().getName(), securityContext.isCallerInRole("user"), securityContext.isCallerInRole("admin"));
        } else {
            meInfo = new UserDto("NOT LOGGED IN", false, false);
        }
        return Response.ok(meInfo).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/change/password")
    public Response updatePassword(PasswordChangeBean passwords) {
        try {
            String currentUser = securityContext.getCallerPrincipal() != null ? securityContext.getCallerPrincipal().getName() : null;
            User user = userService.findByEmail(currentUser);
            if (!userService.checkIfValidOldPassword(user, passwords.getCurrentPassword())) {
                throw new InvalidOldPasswordException("Given password does not match with credentials of the user " + user);
            }
            userService.changeUserPassword(user, passwords.getNewPassword());
            return Response.ok("The password has been updated.").build();
        } catch (InvalidOldPasswordException | UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return Response.status(401, "Password change failed with reason: " + ex.getMessage()).build();
        }
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/forgot/password")
    public Response forgotPassword(PasswordReset passwordReset) {
        try {
            User user = userService.findByEmail(passwordReset.getUserEmail());
            if (user == null) {
                throw new InvalidUserException("The following email does not have an existing account");
            }
            userService.resetUserPassword(user);
            return Response.ok("To reset the password, please check your email").build();
        } catch (InvalidUserException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return Response.status(500, "Password change failed with reason: " + ex.getMessage()).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/reset/password")
    public Response resetPassword(NewPasswordDTO passwordReset) {
        try {
            userService.resetWithToken(passwordReset.getTokenUUID(), passwordReset.getNewPassword());
            return Response.ok("Your password has been changed.").build();
        } catch (InvalidOldPasswordException | UnsupportedEncodingException | NoSuchAlgorithmException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return Response.status(500, "Password change failed with reason: " + ex.getMessage()).build();
        }
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/check/token")
    public Response checkTokenValidity(String token) {
        try {
            String tokenUUID = token.replaceAll("\"", "");
            if (userService.checkTokenValidity(tokenUUID)) {
                return Response.ok("Token is valid").build();
            }
            throw new InvalidTokenException("Token is either invalid or has expired.");
        } catch (InvalidTokenException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return Response.status(401, "Password change failed with reason: " + ex.getMessage()).build();
        }
    }

    @GET
    @Path("private")
    public Response getUserPrivate() {
        UserPrivateDto meInfo;
        if (securityContext.getCallerPrincipal() != null) {
            User user = userService.findByEmail(securityContext.getCallerPrincipal().getName());
            meInfo = new UserPrivateDto(user);
        } else {
            meInfo = new UserPrivateDto("NOT LOGGED IN");
        }
        return Response.ok(meInfo).build();
    }

}
