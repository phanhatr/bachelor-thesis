package cz.cvut.fel.collapp.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "memberships")
public class Membership implements Serializable {

    @EmbeddedId
    private MembershipId membershipId;

    @ManyToOne(optional = false)
    @MapsId("memberships_projectid")
    private Projects projects;

    @ManyToOne(optional = false)
    @MapsId("memberships_userid")
    private User user;

    @Enumerated(EnumType.STRING)
    @Column(name = "memberships_state")
    private MembershipState membershipState;
    @Column(name = "memberships_position")
    private String projectPosition;

    public Membership() {
    }

    public Membership(User user, Projects projects, String projectPosition) {
        this.user = user;
        this.projects = projects;
        this.projectPosition = projectPosition;
        this.membershipState = MembershipState.ACTIVE;
    }

    public Projects getProject() {
        return projects;
    }

    public void setProject(Projects projects) {
        this.projects = projects;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getProjectPosition() {
        return projectPosition;
    }

    public void setProjectPosition(String position) {
        this.projectPosition = position;
    }

    public MembershipState getStatus() {
        return membershipState;
    }

    public void setStatus(MembershipState membershipState) {
        this.membershipState = membershipState;
    }

    public MembershipId getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(MembershipId membershipId) {
        this.membershipId = membershipId;
    }

}
