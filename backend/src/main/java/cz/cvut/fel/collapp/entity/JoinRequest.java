package cz.cvut.fel.collapp.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "joinrequest")
@NamedQueries({
        @NamedQuery(name = "JoinRequest.findById", query = "SELECT j FROM JoinRequest j WHERE j.joinRequestId = :joinRequestId"),
        @NamedQuery(name = "JoinRequest.findByUser", query = "SELECT j from JoinRequest j WHERE j.user = :users"),
        @NamedQuery(name = "JoinRequest.findByProject", query = "SELECT j from JoinRequest j WHERE j.projects = :projects"),})
public class JoinRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer joinRequestId;
    @Column
    private String message;

    @Enumerated(EnumType.STRING)
    private JoinRequestStatus status;

    @Column(nullable = false, updatable = false)
    private Date dateTimeCreated;

    @Column
    private Date dateTimeResolved;

    @ManyToOne(optional = false)
    @JoinColumn(nullable = false)
    private User user;

    @ManyToOne(optional = false)
    @JoinColumn(nullable = false)
    private Projects projects;

    public JoinRequest() {
        this.status = JoinRequestStatus.PENDING;
        this.dateTimeCreated = new Date();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDateTimeCreated() {
        return dateTimeCreated;
    }

    public void setDateTimeCreated(Date dateTimeCreated) {
        this.dateTimeCreated = dateTimeCreated;
    }

    public Date getDateTimeResolved() {
        return dateTimeResolved;
    }

    public void setDateTimeResolved(Date dateTimeResolved) {
        this.dateTimeResolved = dateTimeResolved;
    }

    public JoinRequestStatus getStatus() {
        return status;
    }

    public void setStatus(JoinRequestStatus status) {
        this.status = status;
    }

    public void setJoinRequestId(Integer id) {
        this.joinRequestId = id;
    }

    public Integer getJoinRequestId() {
        return joinRequestId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Projects getProject() {
        return projects;
    }

    public void setProject(Projects projects) {
        this.projects = projects;
    }
}