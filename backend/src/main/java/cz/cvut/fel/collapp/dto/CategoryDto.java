package cz.cvut.fel.collapp.dto;

import cz.cvut.fel.collapp.entity.Category;

public class CategoryDto {
    private Integer categoryId;
    private String categoryName;

    public CategoryDto() {
    }

    public CategoryDto(Category category) {
        this.categoryId = category.getCategoryId();
        this.categoryName = category.getCategoryName();
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
