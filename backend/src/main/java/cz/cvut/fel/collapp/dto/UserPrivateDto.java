package cz.cvut.fel.collapp.dto;

import java.util.List;
import java.util.stream.Collectors;

import cz.cvut.fel.collapp.entity.MembershipState;
import cz.cvut.fel.collapp.entity.User;

public class UserPrivateDto {

    private String email;
    private String firstName;
    private String lastName;
    private List<String> memberships;

    public UserPrivateDto(String email) {
    }

    public UserPrivateDto(User user) {
        this.lastName = user.getEmail();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.memberships = user.getMemberships().stream().filter(m -> m.getStatus() == MembershipState.ACTIVE)
                .map(m -> m.getProject().getProjectId() + m.getProjectPosition()).collect(Collectors.toList());
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<String> getMemberships() {
        return memberships;
    }

    public void setMemberships(List<String> memberships) {
        this.memberships = memberships;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
