package cz.cvut.fel.collapp.exception;

public class InvalidUserException extends CustomException {

    public InvalidUserException(String message) {
        super(message);
    }
}

