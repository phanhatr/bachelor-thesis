package cz.cvut.fel.collapp.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "password_reset")
@NamedQueries({@NamedQuery(name = "PasswordReset.findToken", query = "SELECT p FROM PasswordReset p WHERE p.tokenId = :token_id")})
public class PasswordReset {

    @Id
    @Column(name = "token_id", nullable = false)
    private String tokenId;

    @Basic(optional = false)
    @Column(name = "user_email", nullable = false)
    private String userEmail;

    @Column(name = "expiration_date", nullable = false)
    private Date expirationDate;

    public PasswordReset() {
    }

    public PasswordReset(String tokenId, String userEmail, Date expirationDate) {
        this.tokenId = tokenId;
        this.userEmail = userEmail;
        this.expirationDate = expirationDate;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}
