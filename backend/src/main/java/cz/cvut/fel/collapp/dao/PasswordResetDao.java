package cz.cvut.fel.collapp.dao;

import cz.cvut.fel.collapp.entity.PasswordReset;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;

@Stateless
public class PasswordResetDao extends BaseDao<PasswordReset> {

    public PasswordResetDao() {
        super(PasswordReset.class);
    }

    public PasswordReset findToken(String token) {
        try {
            return em.createNamedQuery("PasswordReset.findToken", PasswordReset.class).setParameter("token_id", token).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}
