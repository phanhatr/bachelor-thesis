package cz.cvut.fel.collapp.service;

import cz.cvut.fel.collapp.dao.MembershipDao;
import cz.cvut.fel.collapp.dao.ProjectDao;
import cz.cvut.fel.collapp.dao.UserDao;
import cz.cvut.fel.collapp.entity.*;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;
import java.util.Objects;

@Stateless
public class MembershipService {
    @EJB
    private MembershipDao membershipDao;
    @EJB
    private UserDao userDao;
    @EJB
    private ProjectDao projectDao;

    public MembershipService() {
    }

    public List<Membership> findAll() {
        return membershipDao.findAll();
    }

    public Membership find(MembershipId id) {
        return membershipDao.find(id);
    }

    public void persist(Membership membership) {
        Objects.requireNonNull(membership);
        membershipDao.persist(membership);
    }

    public void createMembership(Projects projects, User user, String position) {
        Objects.requireNonNull(projects);
        Objects.requireNonNull(user);
        Objects.requireNonNull(position);

        MembershipId membershipId = new MembershipId(projects.getProjectId(), user.getUserId());
        Membership membership = find(membershipId);

        if (membership == null) {
            Membership newMembership = new Membership(user, projects, position);
            projects.addMembership(newMembership);
            user.addMembership(newMembership);

            membershipDao.persist(newMembership);
        } else {
            membership.setStatus(MembershipState.ACTIVE);
            membershipDao.update(membership);
        }
    }

    public void removeUserFromProject(User user, Projects projects) {
        Objects.requireNonNull(user);
        Objects.requireNonNull(projects);

        MembershipId membershipId = new MembershipId(projects.getProjectId(), user.getUserId());
        Membership membership = find(membershipId);
        // should check membership if exists

        membership.setStatus(MembershipState.EXPIRED);

        if (Objects.equals(projects.getProjectCreator().getUserId(), user.getUserId())) {
            membership.setProjectPosition("MEMBER");
            Membership newProjectCreator = projects.getMemberships().get(1);
            newProjectCreator.setProjectPosition("CREATOR");
            projects.setProjectCreator(newProjectCreator.getUser());
            membershipDao.update(newProjectCreator);
        } else {
            for (JoinRequest j : projects.getJoinRequests()) {
                if (j.getUser().getUserId().equals(user.getUserId())) {
                    projects.getJoinRequests().remove(j);
                    break;
                }
            }
        }

        membershipDao.update(membership);
        userDao.update(user);
        projectDao.update(projects);
    }
}
