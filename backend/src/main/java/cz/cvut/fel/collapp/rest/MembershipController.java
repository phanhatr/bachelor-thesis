package cz.cvut.fel.collapp.rest;

import cz.cvut.fel.collapp.dto.JoinRequestDto;
import cz.cvut.fel.collapp.dto.MembershipDto;
import cz.cvut.fel.collapp.entity.*;
import cz.cvut.fel.collapp.exception.ValidationException;
import cz.cvut.fel.collapp.service.JoinRequestService;
import cz.cvut.fel.collapp.service.MembershipService;
import cz.cvut.fel.collapp.service.ProjectService;
import cz.cvut.fel.collapp.service.UserService;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.security.enterprise.SecurityContext;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Path("/memberships")
@Produces(MediaType.APPLICATION_JSON)
public class MembershipController {

    private static final Logger LOG = Logger.getLogger(MembershipController.class.getName());

    @Inject
    SecurityContext securityContext;
    @EJB
    private ProjectService projectService;
    @EJB
    private JoinRequestService joinRequestService;
    @EJB
    private MembershipService membershipService;
    @EJB
    private UserService userService;

    public MembershipController() {
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{projectId}")
    public List<MembershipDto> getMemberships(@PathParam("projectId") Integer projectId) {
        return projectService.findById(projectId).getMemberships().stream()
                .filter(m -> m.getStatus() == MembershipState.ACTIVE).map(MembershipDto::new)
                .collect(Collectors.toList());
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{projectId}/status")
    public JoinRequestStatus getJoinRequestStatus(@PathParam("projectId") Integer projectId) {
        String currentUser = securityContext.getCallerPrincipal() != null ? securityContext.getCallerPrincipal().getName() : null;
        Projects projects = projectService.findById(projectId);
        User user = userService.findByEmail(currentUser);
        return joinRequestService.findPendingJoinRequestsByProjectAndUser(projects, user);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{projectId}/requests")
    public List<JoinRequestDto> getJoinRequests(@PathParam("projectId") Integer projectId) {
        return projectService.findById(projectId).getJoinRequests().stream()
                .filter(j -> j.getStatus() == JoinRequestStatus.PENDING || j.getStatus() == JoinRequestStatus.REJECTED).map(JoinRequestDto::new)
                .collect(Collectors.toList());
    }

    @PUT
    @Path("/{projectId}/requests")
    public Response resolveJoinRequestToProject(@PathParam("projectId") Integer projectId, JoinRequest joinRequest) {
        try {
            Projects projects = projectService.findById(projectId);
            joinRequestService.resolveJoinRequest(projects, joinRequest);
            return Response
                    .ok("Join requests has been handled")
                    .build();
        } catch (ValidationException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return Response.status(500, "Join request handling by creator failed due to: " + ex.getMessage()).build();
        }
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{projectId}/join")
    public Response postJoinRequestToProject(@PathParam("projectId") Integer projectId, JoinRequest joinRequest) {
        try {
            String currentUser = securityContext.getCallerPrincipal() != null ? securityContext.getCallerPrincipal().getName() : null;
            Projects projects = projectService.findById(projectId);
            User user = userService.findByEmail(currentUser);
            joinRequestService.postJoinRequest(joinRequest, projects, user);
            return Response
                    .ok("Join requests has been sent")
                    .build();
        } catch (ValidationException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            return Response.status(500, "Join request failed due to: " + ex.getMessage()).build();
        }
    }

    @PUT
    @Path("/{projectId}/cancel")
    public void cancelJoinRequestToProject(@PathParam("projectId") Integer projectId) {
        String currentUser = securityContext.getCallerPrincipal() != null ? securityContext.getCallerPrincipal().getName() : null;
        Projects projects = projectService.findById(projectId);
        User user = userService.findByEmail(currentUser);
        joinRequestService.cancelJoinRequest(projects, user);
    }

    @DELETE
    @Path("/{projectId}/leave")
    public void leaveProject(@PathParam("projectId") Integer projectId) {
        String currentUser = securityContext.getCallerPrincipal() != null ? securityContext.getCallerPrincipal().getName() : null;
        Projects projects = projectService.findById(projectId);
        User user = userService.findByEmail(currentUser);
        if (Objects.equals(projects.getProjectCreator().getEmail(), currentUser) && projects.getNumberOfCurrentMembers() == 1) {
            projectService.removeProject(projects);
        } else {
            membershipService.removeUserFromProject(user, projects);
        }
    }

    @DELETE
    @Path("/{projectId}/leave/{userId}")
    public void kickUserFromProject(@PathParam("projectId") Integer projectId, @PathParam("userId") Integer userId) {
        User user = userService.find(userId);
        Projects projects = projectService.findById(projectId);
        membershipService.removeUserFromProject(user, projects);
    }
}
